# F4_SWD_Device

#### 介绍
DAP下载器，全速USB2.0，Winusb版本

#### 组成
1.带一个OLDE屏幕显示状态
2.使用的主控为STM32F411CEU 全速usb usb库使用的是HAL库
3.PCB上留有SD卡座和一个SPIFlash位，但目前的版本没有使用它。

#### 速度测试
测试条件:100K的固件(其中50K在外部qspiflash) 目标芯片STM32H750VBT 下载器开启固定速度模式[SWCLK两个跳变沿之间使用 **2个nop延时** (再少就RDDI错误了)]

 ---**耗时:3.7s** 

即使关闭固定速度模式(此时SWCLK速度受到keil设置的控制)，速度设置为5MHz，耗时变为:4.7s。速度设置为2MHz，耗时变为5.4s

换为 STLINK-V2.1 下载,4MHz，耗时为6.9s

