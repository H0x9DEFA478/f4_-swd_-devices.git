/*
 * @Author: 0x9DEFA478
 * @Date: 2021-06-01 20:56:08
 * @LastEditTime: 2021-12-23 16:42:33
 * @LastEditors: 0x9DEFA478
 * @Description: 底层实现 移植时部分需要处理
 * QQ:2652450237
 * ============================================================================================================================================
 * 
 * 
 * 
 *                                                                                               ************      ****************************
 *                                                                                             ************      ****************************  
 *                                                                                           ************      ****************************    
 *                                                                                         ************      ****************************      
 *                                                                                       ************      ************                        
 *                                                                                     ************      ************                          
 *                                                                                   ************      ************                            
 *                                                                                 ************      ************                              
 *                                                                               ************      ************                                
 *                                                                             ************      ************                                  
 *                                                                           ************      ************                                    
 *                                                                         ************      ************                                      
 *                                                                       ************      ************                                        
 *                                                                     ************      ************                                          
 *                                                                   ************      ************                                            
 *                                                                 ************      ************                                              
 *                                                               ************      ************                                                
 *                                                             ************      ************                                                  
 *                                                           ************      ************                                                    
 *                                                         ************      ************                                                      
 *                                                       ************      ************                                                        
 *                                                     ************      ************                                                          
 *                                                   ************      ************                                                            
 *                                                 ************      ************                                                              
 *                                               ************      ************                                                                
 *                                             ************      ************                                                                  
 *                                           ************      ************                                                                    
 *                                         ************      ************                                                                      
 *                                       ************      ************                                                                        
 *                                     ************      ************                                                                          
 *                                   ************      ************                                                                            
 *                                 ************      ************                                                                              
 *                               ************      ************                                                                                
 *                             ************      ************                                                                                  
 *                           ************      ************                                                                                    
 *                         ************      ************                                                                                      
 *       ****************************      ************                                                                                        
 *     ****************************      ************                                                                                          
 *   ****************************      ************                                                                                            
 * ****************************      ************                                                                                              
 * 
 * 
 * 
 * ============================================================================================================================================
 * 
 */
#include "./H_ThreadScheduler_LL.h"
#include "../API/H_ThreadScheduler_Core.h"

extern H_TS* H_TS_Core;


void* volatile * volatile H_TS_RunThread;//调度中断调度判据 指向 (void*)[2] 的内存空间 [0]指向栈顶内存 [1]
void* H_TS_ScheduleISR_Call;//供 LL_ASM使用 调度中断将调用此段指向的方法



#if vH_TS_IsEnableCPULoadCalculate != 0

H_TS_Tick H_TS_GetDT_LastT=0;

#endif




/**
 * @brief 初始化堆栈
 * 根据平台修改
 * @param StackPtr 为堆栈内存首地址指针的指针 初始化完毕后用于返回初始化好的堆栈指针
 * @param StackSize 堆栈大小
 * @param Code 线程执行的代码
 * @param v 线程传入参数
 * @param ReturnCallback 线程方法返回后调用的方法
 * @return 无
 */
void H_TS_ThreadStackInit(void* StackPtr,Hsize StackSize,int (*Code)(void*),void* v,void (*ReturnCallback)(int)){
  //该方法在线程开始时被调用

#if vH_TS_StackPtrDirection == 0

  unsigned int mStackPtr;

  //栈指针指向末尾(刚好超出栈内存空间 代表栈为空)
  ((void**)StackPtr)[0]=(void*)&((Hbyte_ptr)*((void**)StackPtr))[StackSize];

  mStackPtr=((unsigned int*)StackPtr)[0];

  
  mStackPtr-=sizeof(void*);
  (*((void**)mStackPtr))=(void*)0x01000000U;//xPSR

  mStackPtr-=sizeof(void*);
  (*((void**)mStackPtr))=Code;//PC
  
  mStackPtr-=sizeof(void*);
  (*((void**)mStackPtr))=ReturnCallback;//LR 当线程使用return返回 跳转到ReturnCallback

#if 0

  mStackPtr-=sizeof(void*);
  (*((void**)mStackPtr))=ReturnCallback;//R12

  mStackPtr-=sizeof(void*);
  (*((void**)mStackPtr))=ReturnCallback;//R3

  mStackPtr-=sizeof(void*);
  (*((void**)mStackPtr))=ReturnCallback;//R2

  mStackPtr-=sizeof(void*);
  (*((void**)mStackPtr))=ReturnCallback;//R1

  mStackPtr-=sizeof(void*);
#else

  //R1 R2 R3 R12
  mStackPtr-=5*sizeof(void*);

#endif
  
  (*((void**)mStackPtr))=v;//R0


  //下面模拟被保护的现场(实际线程还没开始运行 虚构一个现场)为了线程切换出栈用

  mStackPtr-=sizeof(void*);
  (*((void**)mStackPtr))=(void*)0xFFFFFFFDU;//LR 退回到线程模式 使用PSP

#if 0
  mStackPtr-=sizeof(void*);
  (*((void**)mStackPtr))=(void*)0;//R11

  mStackPtr-=sizeof(void*);
  (*((void**)mStackPtr))=(void*)0;//R10

  mStackPtr-=sizeof(void*);
  (*((void**)mStackPtr))=(void*)0;//R9

  mStackPtr-=sizeof(void*);
  (*((void**)mStackPtr))=(void*)0;//R8

  mStackPtr-=sizeof(void*);
  (*((void**)mStackPtr))=(void*)0;//R7

  mStackPtr-=sizeof(void*);
  (*((void**)mStackPtr))=(void*)0;//R6

  mStackPtr-=sizeof(void*);
  (*((void**)mStackPtr))=(void*)0;//R5

  mStackPtr-=sizeof(void*);
  (*((void**)mStackPtr))=(void*)0;//R4
#else
  mStackPtr-=8*sizeof(void*);
#endif

  ((unsigned int*)StackPtr)[0]=mStackPtr;
#else
#error "未编写"
#endif


}

/**
 * @brief 线程通过return退出时会触发调用的方法
 * @param RetVal 线程的返回值
 * @return 无
 */
void ThreadReturnCallback(int RetVal){
  // H_TS_Thread* _this;

  // _this=cH_TS_RunThread(H_TS_Core)[1];

  //虽然线程通过return返回会转到这里 但其实线程还在运行 只是在执行这里的代码而已
  
  //可以在此截获线程退出信息

  H_TS_ThreadExit(RetVal);//实际上还是要调用线程退出方法
}






