#include "dma.h"


void MX_DMA_Init(void)
{

  /* DMA controller clock enable */
  RCC->AHB1ENR|=RCC_AHB1ENR_DMA1EN;
  RCC->AHB1ENR|=RCC_AHB1ENR_DMA2EN;

  
  HAL_NVIC_SetPriority(DMA2_Stream2_IRQn, 0, 0);
  HAL_NVIC_EnableIRQ(DMA2_Stream2_IRQn);

}


