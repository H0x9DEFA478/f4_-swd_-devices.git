/*
 * @Author: 0x9DEFA478
 * @Date: 2021-08-16 18:44:53
 * @LastEditTime: 2021-08-22 17:10:37
 * @LastEditors: 0x9DEFA478
 * @Description: 
 * QQ:2652450237
 * ============================================================================================================================================
 * 
 * 
 * 
 *                                                                                               ************      ****************************
 *                                                                                             ************      ****************************  
 *                                                                                           ************      ****************************    
 *                                                                                         ************      ****************************      
 *                                                                                       ************      ************                        
 *                                                                                     ************      ************                          
 *                                                                                   ************      ************                            
 *                                                                                 ************      ************                              
 *                                                                               ************      ************                                
 *                                                                             ************      ************                                  
 *                                                                           ************      ************                                    
 *                                                                         ************      ************                                      
 *                                                                       ************      ************                                        
 *                                                                     ************      ************                                          
 *                                                                   ************      ************                                            
 *                                                                 ************      ************                                              
 *                                                               ************      ************                                                
 *                                                             ************      ************                                                  
 *                                                           ************      ************                                                    
 *                                                         ************      ************                                                      
 *                                                       ************      ************                                                        
 *                                                     ************      ************                                                          
 *                                                   ************      ************                                                            
 *                                                 ************      ************                                                              
 *                                               ************      ************                                                                
 *                                             ************      ************                                                                  
 *                                           ************      ************                                                                    
 *                                         ************      ************                                                                      
 *                                       ************      ************                                                                        
 *                                     ************      ************                                                                          
 *                                   ************      ************                                                                            
 *                                 ************      ************                                                                              
 *                               ************      ************                                                                                
 *                             ************      ************                                                                                  
 *                           ************      ************                                                                                    
 *                         ************      ************                                                                                      
 *       ****************************      ************                                                                                        
 *     ****************************      ************                                                                                          
 *   ****************************      ************                                                                                            
 * ****************************      ************                                                                                              
 * 
 * 
 * 
 * ============================================================================================================================================
 * 
 */
#include "I_NVM.h"
#include "Peripheral.h"

#define vI_NVM_DataAddress 0x0800C000U

#if vI_NVM_UseOsLock != 0
#include "H_ThreadScheduler.h"
static H_TS_Lock* I_NVM_Lock=NULL;
#endif


volatile struct _I_NVM I_NVM;



/**
 * @brief 锁定线程锁(使用I_NVM_Init()后可用)
 * @return 无
 */
void I_NVM_Locking(){
  H_TS_LockLock(I_NVM_Lock);
}

/**
 * @brief 解线程锁(使用I_NVM_Init()后可用)
 * @return 无
 */
void I_NVM_UnLock(){
  H_TS_LockUnlock(I_NVM_Lock);
}

/**
 * @brief 保存I_NVM中的数据 (使用I_NVM_Init()后可用 其内部已用I_NVM_Locking()和I_NVM_UnLock())
 * @return 无
 */
void I_NVM_Save(){
  FLASH_EraseInitTypeDef init;
  unsigned int Err;
  unsigned int* p;
  unsigned int Address;
  int i;

  I_NVM_Locking();
  HAL_FLASH_Unlock();
  
  init.Banks=FLASH_BANK_1;
  init.NbSectors=1;
  init.Sector=FLASH_SECTOR_3;
  init.TypeErase=FLASH_TYPEERASE_SECTORS;
  init.VoltageRange=FLASH_VOLTAGE_RANGE_3;
  HAL_FLASHEx_Erase(&init,&Err);

  if(Err==0xFFFFFFFFU){

    p=(unsigned int*)&I_NVM;
    Address=vI_NVM_DataAddress;

    for(i=0;i<((sizeof(struct _I_NVM)+3)/4);i++){
      HAL_FLASH_Program(FLASH_TYPEPROGRAM_WORD,Address,p[i]);
      Address+=4;
    }
  }

  HAL_FLASH_Lock();
  I_NVM_UnLock();
}

/**
 * @brief 初始化NVM
 * @return 无
 */
void I_NVM_Init(){
  int i;

  for(i=0;i<sizeof(struct _I_NVM);i++){
    ((unsigned char*)&I_NVM)[i]=((unsigned char*)vI_NVM_DataAddress)[i];
  }

#if vI_NVM_UseOsLock != 0
  I_NVM_Lock=new_H_TS_Lock();
#endif

  if((I_NVM.TopData!=vI_NVM_TopData)||(I_NVM.EndData!=vI_NVM_EndData)){
    I_NVM.TopData=vI_NVM_TopData;
    I_NVM.EndData=vI_NVM_EndData;

    I_NVM.Screen_Direction=vOLED_Direction_0;
    I_NVM.DAP_IsFast=0;
    I_NVM.DAP_USESoftReset=-1;
  }

}






