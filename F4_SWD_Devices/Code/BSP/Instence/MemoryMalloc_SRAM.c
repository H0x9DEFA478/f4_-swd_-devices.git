/*
 * @Author: 0x9DEFA478
 * @Date: 2021-07-30 14:42:17
 * @LastEditTime: 2021-08-15 23:22:01
 * @LastEditors: 0x9DEFA478
 * @Description: SRAM的内存实现(分配)
 * QQ:2652450237
 * ============================================================================================================================================
 * 
 * 
 * 
 *                                                                                               ************      ****************************
 *                                                                                             ************      ****************************  
 *                                                                                           ************      ****************************    
 *                                                                                         ************      ****************************      
 *                                                                                       ************      ************                        
 *                                                                                     ************      ************                          
 *                                                                                   ************      ************                            
 *                                                                                 ************      ************                              
 *                                                                               ************      ************                                
 *                                                                             ************      ************                                  
 *                                                                           ************      ************                                    
 *                                                                         ************      ************                                      
 *                                                                       ************      ************                                        
 *                                                                     ************      ************                                          
 *                                                                   ************      ************                                            
 *                                                                 ************      ************                                              
 *                                                               ************      ************                                                
 *                                                             ************      ************                                                  
 *                                                           ************      ************                                                    
 *                                                         ************      ************                                                      
 *                                                       ************      ************                                                        
 *                                                     ************      ************                                                          
 *                                                   ************      ************                                                            
 *                                                 ************      ************                                                              
 *                                               ************      ************                                                                
 *                                             ************      ************                                                                  
 *                                           ************      ************                                                                    
 *                                         ************      ************                                                                      
 *                                       ************      ************                                                                        
 *                                     ************      ************                                                                          
 *                                   ************      ************                                                                            
 *                                 ************      ************                                                                              
 *                               ************      ************                                                                                
 *                             ************      ************                                                                                  
 *                           ************      ************                                                                                    
 *                         ************      ************                                                                                      
 *       ****************************      ************                                                                                        
 *     ****************************      ************                                                                                          
 *   ****************************      ************                                                                                            
 * ****************************      ************                                                                                              
 * 
 * 
 * 
 * ============================================================================================================================================
 * 
 */
#include "MemoryMalloc.h"

#define vMemoryMalloc_SRAM_Mem_Size 32*1024
__align(8) static unsigned char MemoryMalloc_SRAM_Mem[vMemoryMalloc_SRAM_Mem_Size];


#if vMemoryMalloc_UseOsLock != 0
#include "H_ThreadScheduler.h"
static H_TS_Lock* MemoryMalloc_SRAM_Lock=NULL;
#endif

/**
 * @brief 分配
 * @param Size 要分配的内存大小, 单位:字节
 * @return 如果为NULL 则分配失败，返回其他则成功
 */
void* MemoryMalloc_SRAM_Malloc(Husize Size){
  void* r;

#if vMemoryMalloc_UseOsLock != 0
  H_TS_LockLock(MemoryMalloc_SRAM_Lock);
#endif
  r=H_Malloc(MemoryMalloc_SRAM_Mem,Size);
#if vMemoryMalloc_UseOsLock != 0
  H_TS_LockUnlock(MemoryMalloc_SRAM_Lock);
#endif

  return r;
}

/**
 * @brief 释放内存
 * @param v 要释放的内存的指针
 * @return 无
 */
void MemoryMalloc_SRAM_Free(void* v){
#if vMemoryMalloc_UseOsLock != 0
  H_TS_LockLock(MemoryMalloc_SRAM_Lock);
#endif
  H_Free(MemoryMalloc_SRAM_Mem,v);
#if vMemoryMalloc_UseOsLock != 0
  H_TS_LockUnlock(MemoryMalloc_SRAM_Lock);
#endif
}

/**
 * @brief SRAM内存初始化
 * @return 无
 */
void MemoryMalloc_SRAM_Init(){

  H_Malloc_Init(MemoryMalloc_SRAM_Mem,vMemoryMalloc_SRAM_Mem_Size);

#if vMemoryMalloc_UseOsLock != 0
  MemoryMalloc_SRAM_Lock=new_H_TS_Lock();
#endif
}

/**
 * @brief SRAM内存信息获取
 * @param info 用于返回信息
 * @return 无
 */
void MemoryMalloc_SRAM_GetInfo(H_Malloc_Info* info){
#if vMemoryMalloc_UseOsLock != 0
  H_TS_LockLock(MemoryMalloc_SRAM_Lock);
#endif
  H_Malloc_GetInfo(MemoryMalloc_SRAM_Mem,info);
#if vMemoryMalloc_UseOsLock != 0
  H_TS_LockUnlock(MemoryMalloc_SRAM_Lock);
#endif
}


