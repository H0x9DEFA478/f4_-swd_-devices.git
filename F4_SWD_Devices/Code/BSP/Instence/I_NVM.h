/*
 * @Author: 0x9DEFA478
 * @Date: 2021-08-16 18:45:21
 * @LastEditTime: 2021-08-22 17:10:33
 * @LastEditors: 0x9DEFA478
 * @Description: 
 * QQ:2652450237
 * ============================================================================================================================================
 * 
 * 
 * 
 *                                                                                               ************      ****************************
 *                                                                                             ************      ****************************  
 *                                                                                           ************      ****************************    
 *                                                                                         ************      ****************************      
 *                                                                                       ************      ************                        
 *                                                                                     ************      ************                          
 *                                                                                   ************      ************                            
 *                                                                                 ************      ************                              
 *                                                                               ************      ************                                
 *                                                                             ************      ************                                  
 *                                                                           ************      ************                                    
 *                                                                         ************      ************                                      
 *                                                                       ************      ************                                        
 *                                                                     ************      ************                                          
 *                                                                   ************      ************                                            
 *                                                                 ************      ************                                              
 *                                                               ************      ************                                                
 *                                                             ************      ************                                                  
 *                                                           ************      ************                                                    
 *                                                         ************      ************                                                      
 *                                                       ************      ************                                                        
 *                                                     ************      ************                                                          
 *                                                   ************      ************                                                            
 *                                                 ************      ************                                                              
 *                                               ************      ************                                                                
 *                                             ************      ************                                                                  
 *                                           ************      ************                                                                    
 *                                         ************      ************                                                                      
 *                                       ************      ************                                                                        
 *                                     ************      ************                                                                          
 *                                   ************      ************                                                                            
 *                                 ************      ************                                                                              
 *                               ************      ************                                                                                
 *                             ************      ************                                                                                  
 *                           ************      ************                                                                                    
 *                         ************      ************                                                                                      
 *       ****************************      ************                                                                                        
 *     ****************************      ************                                                                                          
 *   ****************************      ************                                                                                            
 * ****************************      ************                                                                                              
 * 
 * 
 * 
 * ============================================================================================================================================
 * 
 */
#ifndef __I_NVM_H_
#define __I_NVM_H_


#define vI_NVM_UseOsLock 1

#define vI_NVM_TopData 0x20000607U
#define vI_NVM_EndData 0x67676767U

struct _I_NVM{
  unsigned int TopData;

  int Screen_Direction;
  int DAP_IsFast;
  int DAP_USESoftReset;

  unsigned int EndData;
};


extern volatile struct _I_NVM I_NVM;



/**
 * @brief 锁定线程锁(使用I_NVM_Init()后可用)
 * @return 无
 */
void I_NVM_Locking(void);

/**
 * @brief 解线程锁(使用I_NVM_Init()后可用)
 * @return 无
 */
void I_NVM_UnLock(void);

/**
 * @brief 保存I_NVM中的数据 (使用I_NVM_Init()后可用 其内部已用I_NVM_Locking()和I_NVM_UnLock())
 * @return 无
 */
void I_NVM_Save(void);

/**
 * @brief 初始化NVM
 * @return 无
 */
void I_NVM_Init(void);



#endif //__I_NVM_H_
