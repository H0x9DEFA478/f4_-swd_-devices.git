/*
 * @Author: 0x9DEFA478
 * @Date: 2021-07-30 18:55:53
 * @LastEditTime: 2021-08-16 00:36:05
 * @LastEditors: 0x9DEFA478
 * @Description: 
 * QQ:2652450237
 * ============================================================================================================================================
 * 
 * 
 * 
 *                                                                                               ************      ****************************
 *                                                                                             ************      ****************************  
 *                                                                                           ************      ****************************    
 *                                                                                         ************      ****************************      
 *                                                                                       ************      ************                        
 *                                                                                     ************      ************                          
 *                                                                                   ************      ************                            
 *                                                                                 ************      ************                              
 *                                                                               ************      ************                                
 *                                                                             ************      ************                                  
 *                                                                           ************      ************                                    
 *                                                                         ************      ************                                      
 *                                                                       ************      ************                                        
 *                                                                     ************      ************                                          
 *                                                                   ************      ************                                            
 *                                                                 ************      ************                                              
 *                                                               ************      ************                                                
 *                                                             ************      ************                                                  
 *                                                           ************      ************                                                    
 *                                                         ************      ************                                                      
 *                                                       ************      ************                                                        
 *                                                     ************      ************                                                          
 *                                                   ************      ************                                                            
 *                                                 ************      ************                                                              
 *                                               ************      ************                                                                
 *                                             ************      ************                                                                  
 *                                           ************      ************                                                                    
 *                                         ************      ************                                                                      
 *                                       ************      ************                                                                        
 *                                     ************      ************                                                                          
 *                                   ************      ************                                                                            
 *                                 ************      ************                                                                              
 *                               ************      ************                                                                                
 *                             ************      ************                                                                                  
 *                           ************      ************                                                                                    
 *                         ************      ************                                                                                      
 *       ****************************      ************                                                                                        
 *     ****************************      ************                                                                                          
 *   ****************************      ************                                                                                            
 * ****************************      ************                                                                                              
 * 
 * 
 * 
 * ============================================================================================================================================
 * 
 */
#include "GUI_User.h"
#include "Peripheral.h"

extern unsigned char GUI_User_OLED_Gram[];
extern const unsigned char H0x9DEFA478_Logo[];



/**
 * @brief Add事件输入
 * @return 无
 */
void GUI_User_AddEvent(void);

/**
 * @brief Sub事件输入
 * @return 无
 */
void GUI_User_SubEvent(void);

/**
 * @brief Enter事件输入
 * @return 无
 */
void GUI_User_EnterEvent(void);

/**
 * @brief Back事件输入
 * @return 无
 */
void GUI_User_BackEvent(void);

/**
 * @brief 将图像显示到位图中
 * @param bitmapDst 要显示到的位图
 * @return 无
 */
void GUI_User_Display(H_Graphics_Bitmap* bitmapDst);

/**
 * @brief GUI页面初始化
 * @return 无
 */
void GUI_User_Page_Init(void);






H_Graphics_Bitmap* volatile Bitmap_0x9DEFA478_Logo=NULL;
static H_TS_MessageQueue* volatile GUI_User_delegate=NULL;




static void Button_EventCallback(void* v,Huint32 Event){
  switch (Event&vH_Key_Event_KeyID){
    case vButton_B1:
      if(Event&vH_Key_Event_Up){
        //按键抬起事件
        GUI_User_BackEvent();
      }else if(Event&vH_Key_Event_Press){
        //按键按下事件

      }else if(Event&vH_Key_Event_LongPress){
        //长按事件
        
      }else{
      }
      break;

    case vButton_B2:
      if(Event&vH_Key_Event_Up){
        //按键抬起事件
        GUI_User_EnterEvent();
      }else if(Event&vH_Key_Event_Press){
        //按键按下事件
        
      }else if(Event&vH_Key_Event_LongPress){
        //长按事件
        
      }else{
      }
      break;

    case vButton_B3:
      if(Event&vH_Key_Event_Up){
        //按键抬起事件
        GUI_User_SubEvent();
      }else if(Event&vH_Key_Event_Press){
        //按键按下事件
        
      }else if(Event&vH_Key_Event_LongPress){
        //长按事件
        
      }else{
      }
      break;

    case vButton_B4:
      if(Event&vH_Key_Event_Up){
        //按键抬起事件
        GUI_User_AddEvent();
      }else if(Event&vH_Key_Event_Press){
        //按键按下事件
        
      }else if(Event&vH_Key_Event_LongPress){
        //长按事件
        
      }else{
      }
      break;
    
    default:
      break;
  }
}

H_TS_MessageQueue* Screen_Display_RecvQueue;
H_TS_Thread* Screen_Display_Thread_Handle;
static int Screen_Display_Thread(void* v){
  void* v0;
  void* v1;

  for(;;){
    H_TS_MessageQueueReceive(v,&v0,&v1);
    OLED_Display(v0,v1);
  }
}

/**
 * @brief 向UI线程发起委托 该方法只能由线程调用
 * @param Callee 要执行的代码
 * @param v Callee的传入参数
 * @return 0:成功 其他:失败
 */
int Button_Screen_Thread_BeginInvoke(void (*Callee)(void*),void* v){
  if(GUI_User_delegate==NULL){
    return -1;
  }
  if(H_TS_MessageQueueSend(GUI_User_delegate,Callee,v)!=0){
    return -1;
  }
  return 0;
}

/**
 * @brief UI线程 (管理按键输入与屏幕显示)
 * @param v ...
 * @return 无
 */
int Button_Screen_Thread(void* v){
  void (*Callee)(void*);
  void* Callee_v;
  H_Graphics_Bitmap* b;
  H_Graphics_Bitmap Gram;
  H_Graphics_Area_Param* Param;

  Gram.Bitmap=GUI_User_OLED_Gram;
  Gram.Width=128;
  Gram.Height=64;

  //初始化Logo
  b=MemoryMalloc_SRAM_Malloc(sizeof(H_Graphics_Bitmap));
  b->Bitmap=(void*)H0x9DEFA478_Logo;
  b->Width=70;
  b->Height=40;
  Bitmap_0x9DEFA478_Logo=b;

  //新建队列
  GUI_User_delegate=new_H_TS_MessageQueue(64);

  //初始化按键
  I_Button_Init(Button_EventCallback,NULL);

  //显示Logo
  H_Graphics_L1Vertical_Fill(&Gram,0,0,0,Gram.Width,Gram.Height);
  Param=MemoryMalloc_SRAM_Malloc(sizeof(H_Graphics_Area_Param));
  if(Param==NULL){
    while(1){
    }
  }
  Param->S=Bitmap_0x9DEFA478_Logo;
  Param->D=&Gram;
  Param->sX=0;
  Param->sY=0;
  Param->dX=29;
  Param->dY=4;
  Param->Width=Bitmap_0x9DEFA478_Logo->Width;
  Param->Height=Bitmap_0x9DEFA478_Logo->Height;
  H_Graphics_L1Vertical_CopyTo_L1Vertical(Param);
  OLED_Display(Screen_OLED1,GUI_User_OLED_Gram);
  MemoryMalloc_SRAM_Free(Param);
  H_TS_ThreadSleep(1670);

  GUI_User_Page_Init();

  Screen_Display_RecvQueue=new_H_TS_MessageQueue(4);
  Screen_Display_Thread_Handle=H_TS_StartNewThread_LowPriority(Screen_Display_Thread,Screen_Display_RecvQueue,512,0,0);

  for(;;){

    //线程休眠
    H_TS_ThreadSleep(10);

    while(0==H_TS_MessageQueueTryReceive(GUI_User_delegate,(void**)&Callee,&Callee_v)){
      Callee(Callee_v);//执行委托
    }
    
    I_Button_Loop();
    GUI_User_Display(&Gram);
    H_TS_MessageQueueSend(Screen_Display_RecvQueue,Screen_OLED1,GUI_User_OLED_Gram);
  }


}

