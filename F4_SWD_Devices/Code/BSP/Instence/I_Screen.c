/*
 * @Author: 0x9DEFA478
 * @Date: 2021-07-30 18:10:54
 * @LastEditTime: 2021-08-16 19:30:55
 * @LastEditors: 0x9DEFA478
 * @Description: 
 * QQ:2652450237
 * ============================================================================================================================================
 * 
 * 
 * 
 *                                                                                               ************      ****************************
 *                                                                                             ************      ****************************  
 *                                                                                           ************      ****************************    
 *                                                                                         ************      ****************************      
 *                                                                                       ************      ************                        
 *                                                                                     ************      ************                          
 *                                                                                   ************      ************                            
 *                                                                                 ************      ************                              
 *                                                                               ************      ************                                
 *                                                                             ************      ************                                  
 *                                                                           ************      ************                                    
 *                                                                         ************      ************                                      
 *                                                                       ************      ************                                        
 *                                                                     ************      ************                                          
 *                                                                   ************      ************                                            
 *                                                                 ************      ************                                              
 *                                                               ************      ************                                                
 *                                                             ************      ************                                                  
 *                                                           ************      ************                                                    
 *                                                         ************      ************                                                      
 *                                                       ************      ************                                                        
 *                                                     ************      ************                                                          
 *                                                   ************      ************                                                            
 *                                                 ************      ************                                                              
 *                                               ************      ************                                                                
 *                                             ************      ************                                                                  
 *                                           ************      ************                                                                    
 *                                         ************      ************                                                                      
 *                                       ************      ************                                                                        
 *                                     ************      ************                                                                          
 *                                   ************      ************                                                                            
 *                                 ************      ************                                                                              
 *                               ************      ************                                                                                
 *                             ************      ************                                                                                  
 *                           ************      ************                                                                                    
 *                         ************      ************                                                                                      
 *       ****************************      ************                                                                                        
 *     ****************************      ************                                                                                          
 *   ****************************      ************                                                                                            
 * ****************************      ************                                                                                              
 * 
 * 
 * 
 * ============================================================================================================================================
 * 
 */
#include "I_Screen.h"
#include "Peripherals_Instence.h"
#include "IP_GPIO.h"
#include "H_ThreadScheduler.h"
#include "MemoryMalloc.h"
#include "I_NVM.h"

OLED* Screen_OLED1;
static H_TS_Semaphore* Screen_OLED1_SpiDoneSemaphore;
static int OLED1_Direction;


#define OLED1_SPI_CS_0()      oIP_GPIO_SetOutput_Low(GPIOB,10)
#define OLED1_SPI_CS_1()      oIP_GPIO_SetOutput_High(GPIOB,10)

#define OLED1_SPI_DC_0()      oIP_GPIO_SetOutput_Low(GPIOB,0)
#define OLED1_SPI_DC_1()      oIP_GPIO_SetOutput_High(GPIOB,0)

#define OLED1_SPI_RES_0()     oIP_GPIO_SetOutput_Low(GPIOB,1)
#define OLED1_SPI_RES_1()     oIP_GPIO_SetOutput_High(GPIOB,1)



static void Screen_OLED1_SendDoneCallback(void* v){
  while (IP_SPI_isBusy(ip_spi1))
  {
  }
  H_TS_SemaphoreGive_ISR(Screen_OLED1_SpiDoneSemaphore);
}

static void Reset(){

  OLED1_SPI_RES_1();
	H_TS_ThreadSleep(10);
	OLED1_SPI_RES_0();
	H_TS_ThreadSleep(10);
	OLED1_SPI_RES_1();
  H_TS_ThreadSleep(10);

}


static void SendArray(void* cmd,int cmdlen,void* dat,int datlen,int speed){

  H_TS_LockLock(ip_spi1_Lock);//锁定互斥锁

  if(speed==0){
    IP_SPI_SetSckFreq(ip_spi1,1);
  }else{
    IP_SPI_SetSckFreq(ip_spi1,8);
  }

  IP_SPI_SetTransDoneITCallback(ip_spi1,Screen_OLED1_SendDoneCallback,NULL);
  
  OLED1_SPI_CS_0();
  

  if((cmd!=NULL)&&(cmdlen!=0)){
    OLED1_SPI_DC_0();
    IP_SPI_DMATx_IT_bytes(ip_spi1,cmd,cmdlen);
    H_TS_SemaphoreTake(Screen_OLED1_SpiDoneSemaphore);
  }

  if((dat!=NULL)&&(datlen!=0)){
    OLED1_SPI_DC_1();
    IP_SPI_DMATx_IT_bytes(ip_spi1,dat,datlen);
    H_TS_SemaphoreTake(Screen_OLED1_SpiDoneSemaphore);
  }

  OLED1_SPI_CS_1();

  H_TS_LockUnlock(ip_spi1_Lock);//释放互斥锁

}




/**
 * @brief 获取显示方向
 * @return 显示方向
 */
int I_Screen_GetDirection(){
  return OLED1_Direction;
}

/**
 * @brief 设置显示方向
 * @param Direction 显示方向
 * @return 无
 */
void I_Screen_SetDirection(int Direction){
  OLED1_Direction=Direction;
  OLED_SetDirection(Screen_OLED1,OLED1_Direction);
}

/**
 * @brief 初始化屏幕
 * @return 无
 */
void I_Screen_Init(){
  OLED_LL_Function LL_Func;
  void* cmdArray;

  LL_Func.Reset=Reset;
  LL_Func.SendSequence=SendArray;

  Screen_OLED1_SpiDoneSemaphore=new_H_TS_Semaphore(1);

  Screen_OLED1=MemoryMalloc_SRAM_Malloc(sizeof(OLED));
  if(Screen_OLED1==NULL){
    while(1){
    }
  }

  cmdArray=MemoryMalloc_SRAM_Malloc(8);
  if(cmdArray==NULL){
    while(1){
    }
  }

  OLED_Init(Screen_OLED1,&LL_Func,cmdArray,128,8,vOLED_Type_SSD1306);
	OLED_SetDirection(Screen_OLED1,I_NVM.Screen_Direction);

}










