/*
 * @Author: 0x9DEFA478
 * @Date: 2021-07-31 23:15:09
 * @LastEditTime: 2022-01-08 22:17:12
 * @LastEditors: 0x9DEFA478
 * @Description: 
 * QQ:2652450237
 * ============================================================================================================================================
 * 
 * 
 * 
 *                                                                                               ************      ****************************
 *                                                                                             ************      ****************************  
 *                                                                                           ************      ****************************    
 *                                                                                         ************      ****************************      
 *                                                                                       ************      ************                        
 *                                                                                     ************      ************                          
 *                                                                                   ************      ************                            
 *                                                                                 ************      ************                              
 *                                                                               ************      ************                                
 *                                                                             ************      ************                                  
 *                                                                           ************      ************                                    
 *                                                                         ************      ************                                      
 *                                                                       ************      ************                                        
 *                                                                     ************      ************                                          
 *                                                                   ************      ************                                            
 *                                                                 ************      ************                                              
 *                                                               ************      ************                                                
 *                                                             ************      ************                                                  
 *                                                           ************      ************                                                    
 *                                                         ************      ************                                                      
 *                                                       ************      ************                                                        
 *                                                     ************      ************                                                          
 *                                                   ************      ************                                                            
 *                                                 ************      ************                                                              
 *                                               ************      ************                                                                
 *                                             ************      ************                                                                  
 *                                           ************      ************                                                                    
 *                                         ************      ************                                                                      
 *                                       ************      ************                                                                        
 *                                     ************      ************                                                                          
 *                                   ************      ************                                                                            
 *                                 ************      ************                                                                              
 *                               ************      ************                                                                                
 *                             ************      ************                                                                                  
 *                           ************      ************                                                                                    
 *                         ************      ************                                                                                      
 *       ****************************      ************                                                                                        
 *     ****************************      ************                                                                                          
 *   ****************************      ************                                                                                            
 * ****************************      ************                                                                                              
 * 
 * 
 * 
 * ============================================================================================================================================
 * 
 */
#include "Peripheral.h"





//UI页面句柄
static H_UI* volatile GUI_User_h_ui=NULL;
//正在使用的字符串选项列表
static H_StringItem* volatile GUI_User_h_stringitem=NULL;
//要渲染的位图
static H_Graphics_Bitmap* volatile BitmapDrawString=NULL;

static void L1_DrawString(H_Graphics_Bitmap* bitmap,char* String,H_Font* Font,int X,int Y,int mX,int mY,int isReverse){
  H_Font* rFont;
  int x;
  int y;
  int rWidth;
  unsigned short unicode;
  void* fontBitmap;
  unsigned char c;
  unsigned char c1;
  unsigned char c2;
  H_Graphics_Area_Param Area_Param;
  H_Graphics_Bitmap Src;

  Area_Param.D=bitmap;
  Area_Param.S=&Src;
  x=X;
  y=Y;

  while(String[0]!='\0')
  {
    c=String[0];

    if((c&0x80U)!=0){
      if((c&0xC0U)==0xC0U){
        if((c&0x20U)==0x20U){
        
          if((c&0x10U)==0x10U){
            //不支持过长的UTF8(此时已经超过16Bit)
            String=&String[4];
            continue;
          }else{
            //1110 16Bit

            c1=String[1];
            if((c1&0xC0U)!=0x80U){
              //第二个字节错误
              String=&String[3];
              continue;
            }

            c2=String[2];
            if((c2&0xC0U)!=0x80U){
              //第三个字节错误
              String=&String[3];
              continue;
            }

            unicode=((unsigned short)c&0x0FU)<<12;
            unicode|=((unsigned short)c1&0x3FU)<<6;
            unicode|=c2&0x3FU;
            String=&String[3];
          }

        }else{
          //110 11Bit

          c1=String[1];

          if((c1&0xC0U)!=0x80U){
            //第二个字节错误
            String=&String[2];
            continue;
          }

          unicode=((unsigned short)c&0x1FU)<<6;
          unicode|=c1&0x3FU;

          String=&String[2];
        }
      }else{
        //无效unicode字符
        String=&String[1];
        continue;
      }
    }else{
      //0 7bit
      unicode=c;

      String=&String[1];
    }

    if((unicode=='\r')||(unicode=='\n')){
      String=&String[1];
      while((String[0]=='\r')||(String[0]=='\n')){
        String=&String[1];
      }

      x=0;
      y+=Font->FontHeight;
      if(y>mY){
        return;
      }

      continue;
    }

    rFont=NULL;
    //获取字符
    fontBitmap=Font->GetBitmap(Font,&rFont,unicode,&rWidth);
    if(fontBitmap!=NULL){
      if(rFont==NULL){
        while(1)
        {
          //Err
        }
      }

      if(rWidth==rFont->FontMaxWidth-1){
        rWidth=rFont->FontMaxWidth;
      }

      Src.Bitmap=fontBitmap;
      Src.Height=rFont->FontHeight;
      Src.Width=rFont->FontMaxWidth;
      Area_Param.sX=0;
      Area_Param.sY=0;

      Area_Param.dX=x;
      Area_Param.dY=y;

      if((x+rWidth-1)>mX){
        Area_Param.Width=mX+1-x;
      }else{
        Area_Param.Width=rWidth;
      }
      if(Area_Param.Width<0){
        return;
      }

      if((y+rFont->FontHeight-1)>mY)
      {
        Area_Param.Height=mY+1-y;
      }else{
        Area_Param.Height=rFont->FontHeight;
      }

      if(isReverse!=0){
        H_Graphics_L1Vertical_CopyTo_L1Vertical_Reverse(&Area_Param);
      }else{
        H_Graphics_L1Vertical_CopyTo_L1Vertical(&Area_Param);
      }

      x+=rWidth;
    }

  }

}

static void DrawString(char *String, void *Font, int Status, int X, int Y, int MaxX, int MaxY){
  H_Graphics_Bitmap* bitmap;

  if(BitmapDrawString==NULL){
    return;
  }

  bitmap=BitmapDrawString;

  switch(Status){
    case vH_StringItem_Status_Default:
      L1_DrawString(bitmap,String,Font,X,Y,MaxX,MaxY,0);
      break;
    case vH_StringItem_Status_HighLight:
      L1_DrawString(bitmap,String,Font,X,Y,MaxX,MaxY,-1);
      break;
    case vH_StringItem_Status_Select:
      L1_DrawString(bitmap,String,Font,X,Y,MaxX,MaxY,-1);
      break;
    
    default:
      break;
  }
}


//============================================================================================================================================
//页面1
static H_UI_Page* h_ui_page1=NULL;
static H_StringItem* volatile h_ui_page1_h_stringitem=NULL;
static H_StringItem_List* volatile h_ui_page1_h_stringitem_List1=NULL;

static H_StringItem_ListItem* volatile h_ui_page1_h_stringitem_List1_Item1=NULL;
static H_StringItem_ListItem* volatile h_ui_page1_h_stringitem_List1_Item2=NULL;
static H_StringItem_ListItem* volatile h_ui_page1_h_stringitem_List1_Item3=NULL;
static H_StringItem_ListItem* volatile h_ui_page1_h_stringitem_List1_Save=NULL;

static void h_ui_page1_h_stringitem_List1_ExitCallback(H_StringItem_List* list){
  GUI_User_h_stringitem=NULL;
  H_StringItem_SetString(h_ui_page1_h_stringitem_List1_Save,"  保存设置      ");
}

static void h_ui_page1_h_stringitem_Listx_Itemx_SelectCallback(H_StringItem_ListItem* item){
  item->String[0]='-';
  item->String[1]='>';
}

static void h_ui_page1_h_stringitem_Listx_Itemx_DeSelectCallback(H_StringItem_ListItem* item){
  item->String[0]=' ';
  item->String[1]=' ';
}

static void h_ui_page1_h_stringitem_List1_Item1_AddClickCallback(H_StringItem_ListItem* item){
  I_NVM_Locking();
  if(DAP_IsFastSpeed==0){
    DAP_IsFastSpeed=-1;
    I_NVM.DAP_IsFast=-1;
    H_StringItem_SetString(item,"->固定最高速  是");
  }else{
    DAP_IsFastSpeed=0;
    I_NVM.DAP_IsFast=0;
    H_StringItem_SetString(item,"->固定最高速  否");
  }
  I_NVM_UnLock();
}

static void h_ui_page1_h_stringitem_List1_Item2_AddClickCallback(H_StringItem_ListItem* item){
  I_NVM_Locking();
  if(I_NVM.Screen_Direction==vOLED_Direction_0){
    I_NVM.Screen_Direction=vOLED_Direction_180;
    OLED_SetDirection(Screen_OLED1,vOLED_Direction_180);
    H_StringItem_SetString(item,"->屏幕方向    反");
  }else{
    I_NVM.Screen_Direction=vOLED_Direction_0;
    OLED_SetDirection(Screen_OLED1,vOLED_Direction_0);
    H_StringItem_SetString(item,"->屏幕方向    正");
  }
  I_NVM_UnLock();
}

static void h_ui_page1_h_stringitem_List1_Item3_AddClickCallback(H_StringItem_ListItem* item){
  I_NVM_Locking();
  if(I_NVM.DAP_USESoftReset==0){
    I_NVM.DAP_USESoftReset=-1;
    H_StringItem_SetString(h_ui_page1_h_stringitem_List1_Item3,"->软件复位  使用");
  }else{
    I_NVM.DAP_USESoftReset=0;
    H_StringItem_SetString(h_ui_page1_h_stringitem_List1_Item3,"->软件复位不使用");
  }
  I_NVM_UnLock();
}

static void h_ui_page1_h_stringitem_List1_Save_AddClickCallback(H_StringItem_ListItem* item){

  I_NVM_Save();
  H_StringItem_SetString(item,"->保存设置  完成");
}

static void h_ui_page1_EnterPageCallback(void* page){
}

static void h_ui_page1_ExitPageCallback(void* page){
  H_Graphics_L1Vertical_Fill(BitmapDrawString,0,0,0,BitmapDrawString->Width,BitmapDrawString->Height);
}

static void h_ui_page1_DisplayPageCallback(void* page){
  char str[32];
  extern long long transBytes;
  int speed;
  
  H_StringItem* stringitem;
  stringitem=GUI_User_h_stringitem;
  if(stringitem!=NULL){
    H_Graphics_L1Vertical_Fill(BitmapDrawString,0,0,0,BitmapDrawString->Width,BitmapDrawString->Height);
    H_StringItem_Draw(stringitem);
  }else{
    H_Graphics_L1Vertical_Fill(BitmapDrawString,0,0,0,BitmapDrawString->Width,BitmapDrawString->Height);
    DrawString("当前状态",Font_LLib_Song,0,32,0,127,63);

    if(usbd_class_IsConnect()==0){
      DrawString("USB_NO_CONNECT",Font_ASCII_H,0,0,16,127,63);
    }else{
      DrawString("USB_CONNECTED",Font_ASCII_H,0,0,16,127,63);
    }

    if(usbd_class_IsConnect()==0){
      DrawString("USB_NO_CONNECT",Font_ASCII_H,0,0,16,127,63);
    }else{
      DrawString("USB_CONNECTED",Font_ASCII_H,0,0,16,127,63);
    }

    if(DAP_IsConnected==0){
      DrawString("NO_CONNECT",Font_ASCII_H,0,0,24,127,63);
    }else{
      DrawString("CONNECTED",Font_ASCII_H,0,2,24,127,63);
    }

    if(DAP_IsRunning==0){
      DrawString("STOP",Font_ASCII_H,0,64,24,127,63);
    }else{
      DrawString("RUNNING",Font_ASCII_H,0,64,24,127,63);
    }

    DrawString(H_String(str,"CPU:{f.1}%",(float)H_TS_GetCPU_Load()/10.0f),Font_ASCII_H,0,0,32,127,63);

    speed=DAP_InSpeed;
    if(speed<1024){
      DrawString(H_String(str,"IN:{i}B/S",speed),Font_ASCII_H,0,0,40,127,63);
    }else{
      DrawString(H_String(str,"IN:{f.2}KB/S",(float)speed/1024.0f),Font_ASCII_H,0,0,40,127,63);
    }

    speed=DAP_OutSpeed;
    if(speed<1024){
      DrawString(H_String(str,"OUT:{i}B/S",speed),Font_ASCII_H,0,0,48,127,63);
    }else{
      DrawString(H_String(str,"OUT:{f.2}KB/S",(float)speed/1024.0f),Font_ASCII_H,0,0,48,127,63);
    }



    
  }
}

static void h_ui_page1_h_stringitem_init(){

  h_ui_page1_h_stringitem=new_H_StringItem(DrawString,MemoryMalloc_SRAM_Malloc,MemoryMalloc_SRAM_Free);
  h_ui_page1_h_stringitem_List1=new_H_StringItem_List(h_ui_page1_h_stringitem,0,0,128,64);

  h_ui_page1_h_stringitem_List1_Item1=H_StringItem_List_AddItem(h_ui_page1_h_stringitem_List1,16);
  h_ui_page1_h_stringitem_List1_Item2=H_StringItem_List_AddItem(h_ui_page1_h_stringitem_List1,16);
  h_ui_page1_h_stringitem_List1_Item3=H_StringItem_List_AddItem(h_ui_page1_h_stringitem_List1,16);
  h_ui_page1_h_stringitem_List1_Save=H_StringItem_List_AddItem(h_ui_page1_h_stringitem_List1,16);
  
  H_StringItem_SetFont(h_ui_page1_h_stringitem_List1_Item1,Font_LLib_Song);
  H_StringItem_SetFont(h_ui_page1_h_stringitem_List1_Item2,Font_LLib_Song);
  H_StringItem_SetFont(h_ui_page1_h_stringitem_List1_Item3,Font_LLib_Song);
  H_StringItem_SetFont(h_ui_page1_h_stringitem_List1_Save,Font_LLib_Song);

  if(I_NVM.DAP_IsFast==0){
    H_StringItem_SetString(h_ui_page1_h_stringitem_List1_Item1,"  固定最高速  否");
  }else{
    H_StringItem_SetString(h_ui_page1_h_stringitem_List1_Item1,"  固定最高速  是");
  }

  if(I_NVM.Screen_Direction==vOLED_Direction_0){
    H_StringItem_SetString(h_ui_page1_h_stringitem_List1_Item2,"  屏幕方向    正");
  }else{
    H_StringItem_SetString(h_ui_page1_h_stringitem_List1_Item2,"  屏幕方向    反");
  }

  if(I_NVM.DAP_USESoftReset!=0){
    H_StringItem_SetString(h_ui_page1_h_stringitem_List1_Item3,"  软件复位  使用");
  }else{
    H_StringItem_SetString(h_ui_page1_h_stringitem_List1_Item3,"  软件复位不使用");
  }

  H_StringItem_SetString(h_ui_page1_h_stringitem_List1_Save,"  保存设置      ");


  h_ui_page1_h_stringitem_List1->ExitCallback=h_ui_page1_h_stringitem_List1_ExitCallback;

  h_ui_page1_h_stringitem_List1_Item1->SelectCallback=h_ui_page1_h_stringitem_Listx_Itemx_SelectCallback;
  h_ui_page1_h_stringitem_List1_Item1->DeSelectCallback=h_ui_page1_h_stringitem_Listx_Itemx_DeSelectCallback;
  h_ui_page1_h_stringitem_List1_Item1->AddClickCallback=h_ui_page1_h_stringitem_List1_Item1_AddClickCallback;

  h_ui_page1_h_stringitem_List1_Item2->SelectCallback=h_ui_page1_h_stringitem_Listx_Itemx_SelectCallback;
  h_ui_page1_h_stringitem_List1_Item2->DeSelectCallback=h_ui_page1_h_stringitem_Listx_Itemx_DeSelectCallback;
  h_ui_page1_h_stringitem_List1_Item2->AddClickCallback=h_ui_page1_h_stringitem_List1_Item2_AddClickCallback;

  h_ui_page1_h_stringitem_List1_Item3->SelectCallback=h_ui_page1_h_stringitem_Listx_Itemx_SelectCallback;
  h_ui_page1_h_stringitem_List1_Item3->DeSelectCallback=h_ui_page1_h_stringitem_Listx_Itemx_DeSelectCallback;
  h_ui_page1_h_stringitem_List1_Item3->AddClickCallback=h_ui_page1_h_stringitem_List1_Item3_AddClickCallback;

  h_ui_page1_h_stringitem_List1_Save->SelectCallback=h_ui_page1_h_stringitem_Listx_Itemx_SelectCallback;
  h_ui_page1_h_stringitem_List1_Save->DeSelectCallback=h_ui_page1_h_stringitem_Listx_Itemx_DeSelectCallback;
  h_ui_page1_h_stringitem_List1_Save->AddClickCallback=h_ui_page1_h_stringitem_List1_Save_AddClickCallback;

  H_StringItem_SetList(h_ui_page1_h_stringitem,h_ui_page1_h_stringitem_List1);

}

//============================================================================================================================================
//页面2
static H_UI_Page* h_ui_page2=NULL;

static void h_ui_page2_EnterPageCallback(void* page){
}

static void h_ui_page2_ExitPageCallback(void* page){
  H_Graphics_L1Vertical_Fill(BitmapDrawString,0,0,0,BitmapDrawString->Width,BitmapDrawString->Height);
}

static void h_ui_page2_DisplayPageCallback(void* page){
  H_StringItem* stringitem;
  char* str[32];
  H_Malloc_Info sram_info;
 
  stringitem=GUI_User_h_stringitem;


  MemoryMalloc_SRAM_GetInfo(&sram_info);
  


  if(stringitem!=NULL){
    H_Graphics_L1Vertical_Fill(BitmapDrawString,0,0,0,BitmapDrawString->Width,BitmapDrawString->Height);
    H_StringItem_Draw(stringitem);
  }else{
    H_Graphics_L1Vertical_Fill(BitmapDrawString,0,0,0,BitmapDrawString->Width,BitmapDrawString->Height);
    DrawString("其他信息",Font_LLib_Song,0,0,0,127,64);

    if(sram_info.Result==0){
      DrawString(H_String(str,"SRAM:{i}/{i}",sram_info.UseSize,sram_info.OccupySize+sram_info.NoOccupySize),Font_ASCII_H,0,0,24,127,64);
    }else{
      DrawString(H_String(str,"SRAM:Err:{i} addr:0x{X8}",sram_info.Result,(unsigned int)sram_info.ErrPtr),Font_ASCII_H,0,0,24,127,64);
    }

    DrawString(H_String(str,"H_TS_STACK:{f.1}%",(float)H_TS_GetStackUtilization()/10.0f),Font_ASCII_H,0,0,32,127,64);

  }
}

static void h_ui_page2_h_stringitem_init(){

  

}








//============================================================================================================================================
//对外提供方法 (UI线程使用)

/**
 * @brief Add事件输入
 * @return 无
 */
void GUI_User_AddEvent(){
  H_StringItem* stringitem;
  stringitem=GUI_User_h_stringitem;

  if(stringitem==NULL){
    H_UI_Page_NextPage(GUI_User_h_ui);
  }else{
    H_StringItem_EventAdd(stringitem);
  }
}

/**
 * @brief Sub事件输入
 * @return 无
 */
void GUI_User_SubEvent(){
  H_StringItem* stringitem;
  stringitem=GUI_User_h_stringitem;

  if(stringitem==NULL){
    H_UI_Page_LastPage(GUI_User_h_ui);
  }else{
    H_StringItem_EventSub(stringitem);
  }
}

/**
 * @brief Enter事件输入
 * @return 无
 */
void GUI_User_EnterEvent(){
  H_StringItem* stringitem;
  stringitem=GUI_User_h_stringitem;

  if(stringitem==NULL){
    GUI_User_h_stringitem=GUI_User_h_ui->Page->UserInstence;
  }else{
    H_StringItem_EventEnter(stringitem);
  }
}

/**
 * @brief Back事件输入
 * @return 无
 */
void GUI_User_BackEvent(){
  H_StringItem* stringitem;
  stringitem=GUI_User_h_stringitem;

  if(stringitem==NULL){
    
  }else{
    H_StringItem_EventBack(stringitem);
  }
}

/**
 * @brief 将图像显示到位图中
 * @param bitmapDst 要显示到的位图
 * @return 无
 */
void GUI_User_Display(H_Graphics_Bitmap* bitmapDst){

  BitmapDrawString=bitmapDst;

  if(GUI_User_h_ui==NULL){
    return;
  }
  H_UI_Page_Display(GUI_User_h_ui);

}

/**
 * @brief GUI页面初始化
 * @return 无
 */
void GUI_User_Page_Init(){
  H_UI_Callbacks Callbacks;
  H_UI_Page* page;

  GUI_User_h_ui=new_H_UI(MemoryMalloc_SRAM_Malloc,MemoryMalloc_SRAM_Free);
  if(GUI_User_h_ui==NULL){
    for(;;){
    }
  }
  
  h_ui_page1_h_stringitem_init();
  Callbacks.EnterPageCallback=h_ui_page1_EnterPageCallback;
  Callbacks.ExitPageCallback=h_ui_page1_ExitPageCallback;
  Callbacks.DisplayPageCallback=h_ui_page1_DisplayPageCallback;
  page=H_UI_NewPage(GUI_User_h_ui,H_UI_GetEmptyPage(GUI_User_h_ui),&Callbacks);
  if(page==NULL){
    for(;;){
    }
  }
  page->UserInstence=h_ui_page1_h_stringitem;
  h_ui_page1=page;

  h_ui_page2_h_stringitem_init();
  Callbacks.EnterPageCallback=h_ui_page2_EnterPageCallback;
  Callbacks.ExitPageCallback=h_ui_page2_ExitPageCallback;
  Callbacks.DisplayPageCallback=h_ui_page2_DisplayPageCallback;
  page=H_UI_NewPage(GUI_User_h_ui,page,&Callbacks);
  if(page==NULL){
    for(;;){
    }
  }
  page->UserInstence=NULL;
  h_ui_page2=page;

  

}





//============================================================================================================================================
//用户方法定义 供外部使用其内部的部件(因为部件被标为static 只能在这里使用)



