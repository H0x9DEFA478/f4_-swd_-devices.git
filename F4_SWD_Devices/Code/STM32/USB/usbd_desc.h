#ifndef __USBD_DESC_TEMPLATE_H
#define __USBD_DESC_TEMPLATE_H
#include "usbd_def.h"


#define         DEVICE_ID1          (UID_BASE)
#define         DEVICE_ID2          (UID_BASE + 0x4U)
#define         DEVICE_ID3          (UID_BASE + 0x8U)



#define USB_SIZ_STRING_SERIAL       0x1AU
#define USB_MS_VendorCode           0x67U




extern USBD_DescriptorsTypeDef USB1_Desc; /* Replace 'XXX_Desc' with your active USB device class, ex: HID_Desc */


/**
 * @brief 获取 MS_OS_20_DescriptorSet
 * @param speed Current device speed
 * @param length Pointer to data length variable
 * @return Pointer to descriptor buffer
 */
unsigned char* USBD_Class_MS_OS_20_DescriptorSet(USBD_SpeedTypeDef speed,unsigned int* length);


#endif /* __USBD_DESC_TEMPLATE_H*/
