#include "usb_device.h"
#include "usbd_core.h"
#include "usbd_desc.h"
#include "usbd_class.h"



USBD_HandleTypeDef hUsbDeviceFS1;

void MX_USB_DEVICE_Init(void){

  if (USBD_Init(&hUsbDeviceFS1, &USB1_Desc, 0) != USBD_OK)
  {
    Error_Handler();
  }
  if (USBD_RegisterClass(&hUsbDeviceFS1, &USBD_Class_Driver) != USBD_OK)
  {
    Error_Handler();
  }
  if (USBD_Start(&hUsbDeviceFS1) != USBD_OK)
  {
    Error_Handler();
  }


}

