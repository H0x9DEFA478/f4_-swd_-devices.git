/*
 * @Author: 0x9DEFA478
 * @Date: 2021-08-07 20:07:50
 * @LastEditTime: 2021-08-16 00:45:52
 * @LastEditors: 0x9DEFA478
 * @Description: 
 * QQ:2652450237
 * ============================================================================================================================================
 * 
 * 
 * 
 *                                                                                               ************      ****************************
 *                                                                                             ************      ****************************  
 *                                                                                           ************      ****************************    
 *                                                                                         ************      ****************************      
 *                                                                                       ************      ************                        
 *                                                                                     ************      ************                          
 *                                                                                   ************      ************                            
 *                                                                                 ************      ************                              
 *                                                                               ************      ************                                
 *                                                                             ************      ************                                  
 *                                                                           ************      ************                                    
 *                                                                         ************      ************                                      
 *                                                                       ************      ************                                        
 *                                                                     ************      ************                                          
 *                                                                   ************      ************                                            
 *                                                                 ************      ************                                              
 *                                                               ************      ************                                                
 *                                                             ************      ************                                                  
 *                                                           ************      ************                                                    
 *                                                         ************      ************                                                      
 *                                                       ************      ************                                                        
 *                                                     ************      ************                                                          
 *                                                   ************      ************                                                            
 *                                                 ************      ************                                                              
 *                                               ************      ************                                                                
 *                                             ************      ************                                                                  
 *                                           ************      ************                                                                    
 *                                         ************      ************                                                                      
 *                                       ************      ************                                                                        
 *                                     ************      ************                                                                          
 *                                   ************      ************                                                                            
 *                                 ************      ************                                                                              
 *                               ************      ************                                                                                
 *                             ************      ************                                                                                  
 *                           ************      ************                                                                                    
 *                         ************      ************                                                                                      
 *       ****************************      ************                                                                                        
 *     ****************************      ************                                                                                          
 *   ****************************      ************                                                                                            
 * ****************************      ************                                                                                              
 * 
 * 
 * 
 * ============================================================================================================================================
 * 
 */
#include "usbd_class_if.h"
#include "usbd_class.h"



extern USBD_HandleTypeDef hUsbDeviceFS1;

//============================================================================================================================================
//cdc相关请求定义
//============================================================================================================================================

#define CDC_SEND_ENCAPSULATED_COMMAND                              0x00U
#define CDC_GET_ENCAPSULATED_RESPONSE                              0x01U
#define CDC_SET_COMM_FEATURE                                       0x02U
#define CDC_GET_COMM_FEATURE                                       0x03U
#define CDC_CLEAR_COMM_FEATURE                                     0x04U
#define CDC_SET_AUX_LINE_STATE                                     0x10U
#define CDC_SET_HOOK_STATE                                         0x11U
#define CDC_PULSE_SETUP                                            0x12U
#define CDC_SEND_PULSE                                             0x13U
#define CDC_SET_PULSE_TIME                                         0x14U
#define CDC_RING_AUX_JACK                                          0x15U
#define CDC_SET_LINE_CODING                                        0x20U
#define CDC_GET_LINE_CODING                                        0x21U
#define CDC_SET_CONTROL_LINE_STATE                                 0x22U
#define CDC_SEND_BREAK                                             0x23U
#define CDC_SET_RINGER_PARMS                                       0x30U
#define CDC_GET_RINGER_PARMS                                       0x31U
#define CDC_SET_OPERATION_PARMS                                    0x32U
#define CDC_GET_OPERATION_PARMS                                    0x33U
#define CDC_SET_LINE_PARMS                                         0x34h
#define CDC_GET_LINE_PARMS                                         0x35h
#define CDC_DIAL_DIGITS                                            0x36h
#define CDC_SET_UNIT_PARAMETER                                     0x37h
#define CDC_GET_UNIT_PARAMETER                                     0x38h
#define CDC_CLEAR_UNIT_PARAMETER                                   0x39h
#define CDC_GET_PROFILE                                            0x3Ah
#define CDC_SET_ETHERNET_MULTICAST_FILTERS                         0x40h
#define CDC_SET_ETHERNET_POWER_MANAGEMENT_PATTERN_FILTER           0x41h
#define CDC_GET_ETHERNET_POWER_MANAGEMENT_PATTERN_FILTER           0x42h
#define CDC_SET_ETHERNET_PACKET_FILTER                             0x43h
#define CDC_GET_ETHERNET_STATISTIC                                 0x44h
#define CDC_SET_ATM_DATA_FORMAT                                    0x50h
#define CDC_GET_ATM_DEVICE_STATISTICS                              0x51h
#define CDC_SET_ATM_DEFAULT_VC                                     0x52h
#define CDC_GET_ATM_VC_STATISTICS                                  0x53h





/**
 * @brief usb连接回调
 * @param CfgId 配置号
 * @return 无
 */
void usbd_class_ConnectCallback(int CfgId){

}

/**
 * @brief usb断开连接回调
 * @param CfgId 配置号
 * @return 无
 */
void usbd_class_DisConnectCallback(int CfgId){
  
}

/**
 * @brief Winusb_CMSIS_DAP接收回调 被USB中断调用
 * @param Data 接收到的数据
 * @param Length 接收到的数据大小
 * @return 无
 */
void usbd_class_Winusb_CMSIS_DAP_RecvCallback(unsigned char* Data,int Length){

  DAP_Thread_InputData(Data,Length);
}

/**
 * @brief Winusb_CMSIS_DAP发送完成回调 被USB中断调用
 * @return 无
 */
void usbd_class_Winusb_CMSIS_DAP_SendDoneCallback(){
  DAP_Thread_TransferDone();
}

/**
 * @brief CDC接收回调 被USB中断调用
 * @param Data 接收到的数据
 * @param Length 接收到的数据大小
 * @return 无
 */
void usbd_class_CDC_RecvCallback(unsigned char* Data,int Length){

  usbd_class_CDC_Send(Data,Length);
}

/**
 * @brief CDC发送完成回调 被USB中断调用
 * @return 无
 */
void usbd_class_CDC_SendDoneCallback(){

}

/**
 * @brief CDC请求回调
 * @param cmd 请求 bRequest
 * @param data 数据
 * @param Length 数据长度
 * @return 无
 */
void usbd_class_CDC_Ctrl_Callback(unsigned char cmd,unsigned char* data,int Length){
  static unsigned int dwDTERate=115200;
  static unsigned char bCharFormat=0;//0:1 Stop bit 2:2 Stop bits 1:1.5 Stop bits
  static unsigned char bParityType=0;//0:None 1: 2:Even 3:Mark 4:Space
  static unsigned char bDataBits=8;

  switch(cmd){
    case CDC_SEND_ENCAPSULATED_COMMAND:
      break;
    case CDC_GET_ENCAPSULATED_RESPONSE:
      break;
    case CDC_SET_COMM_FEATURE:
      break;
    case CDC_GET_COMM_FEATURE:
      break;
    case CDC_CLEAR_COMM_FEATURE:
      break;
    case CDC_SET_LINE_CODING:
      if(Length>=7){
        ((unsigned char*)&dwDTERate)[0]=data[0];
        ((unsigned char*)&dwDTERate)[1]=data[1];
        ((unsigned char*)&dwDTERate)[2]=data[2];
        ((unsigned char*)&dwDTERate)[3]=data[3];
        bCharFormat=data[4];
        bParityType=data[5];
        bDataBits=data[6];
      }
      
      break;
    case CDC_GET_LINE_CODING:
      if(Length>=7){
        data[0]=((unsigned char*)&dwDTERate)[0];
        data[1]=((unsigned char*)&dwDTERate)[1];
        data[2]=((unsigned char*)&dwDTERate)[2];
        data[3]=((unsigned char*)&dwDTERate)[3];
        data[4]=bCharFormat;
        data[5]=bParityType;
        data[6]=bDataBits;
      }
      break;
    case CDC_SET_CONTROL_LINE_STATE:
      break;
    case CDC_SEND_BREAK:
      break;
    
    default:
      break;
  }

}










/**
 * @brief usb是否连接
 * @return 0: 无连接 其他: 连接
 */
int usbd_class_IsConnect(){
  if(usbd_class_GetConfigurationNum_m()==0){
    return 0;
  }
  return -1;
}

/**
 * @brief 发送数据 
 * @param Data 要发送的数据 此地址要4字节对齐
 * @param Length 发送的数据长度
 * @return 0:成功 其他:失败
 */
int usbd_class_Winusb_CMSIS_DAP_Send(unsigned char* Data,int Length){
  return usbd_class_Winusb_CMSIS_DAP_Send_m(&hUsbDeviceFS1,Data,Length);
}

/**
 * @brief 检查发送是否忙碌
 * @return 0:空闲 其他:忙碌
 */
int usbd_class_Winusb_CMSIS_DAP_IsBusy(){
  return usbd_class_Winusb_CMSIS_DAP_IsBusy_m(&hUsbDeviceFS1);
}

/**
 * @brief 发送数据 
 * @param Data 要发送的数据 此地址要4字节对齐
 * @param Length 发送的数据长度
 * @return 0:成功 其他:失败
 */
int usbd_class_CDC_Send(unsigned char* Data,int Length){
  return usbd_class_CDC_Send_m(&hUsbDeviceFS1,Data,Length);
}

/**
 * @brief 检查发送是否忙碌
 * @return 0:空闲 其他:忙碌
 */
int usbd_class_CDC_IsBusy(){
  return usbd_class_CDC_IsBusy_m(&hUsbDeviceFS1);
}


