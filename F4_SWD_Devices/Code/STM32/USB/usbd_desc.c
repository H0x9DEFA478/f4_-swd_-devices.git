#include "usbd_core.h"
#include "usbd_desc.h"
#include "usbd_conf.h"


#define USBD_VID                      0xC251
#define USBD_PID                      0xF00A  
#define USBD_LANGID_STRING            1033 //EN:1033
#define USBD_MANUFACTURER_STRING      "0x9DEFA478" /* Add your manufacturer string */
#define USBD_CONFIGURATION_HS_STRING  "Default_CONFIGURATION_HS" /* Add your configuration High Speed string */
#define USBD_INTERFACE_HS_STRING      "Default_Interface_HS" /* Add your Interface High Speed string */
#define USBD_CONFIGURATION_FS_STRING  "Default_CONFIGURATION_HS" /* Add your configuration Full Speed string */
#define USBD_INTERFACE_FS_STRING      "Default_Interface_FS" /* Add your Interface Full Speed string */

/* Private macro -------------------------------------------------------------*/
/* Private function prototypes -----------------------------------------------*/
uint8_t *USBD_Class_DeviceDescriptor(USBD_SpeedTypeDef speed, uint16_t *length);
uint8_t *USBD_Class_LangIDStrDescriptor(USBD_SpeedTypeDef speed, uint16_t *length);
uint8_t *USBD_Class_ManufacturerStrDescriptor(USBD_SpeedTypeDef speed, uint16_t *length);
uint8_t *USBD_Class_ProductStrDescriptor(USBD_SpeedTypeDef speed, uint16_t *length);
uint8_t *USBD_Class_SerialStrDescriptor(USBD_SpeedTypeDef speed, uint16_t *length);
uint8_t *USBD_Class_ConfigStrDescriptor(USBD_SpeedTypeDef speed, uint16_t *length);
uint8_t *USBD_Class_InterfaceStrDescriptor(USBD_SpeedTypeDef speed, uint16_t *length);

#if (USBD_CLASS_USER_STRING_DESC == 1)
uint8_t *USBD_Class_UserStrDescriptor(USBD_SpeedTypeDef speed, uint8_t idx, uint16_t *length);
#endif /* USB_CLASS_USER_STRING_DESC */

#if ((USBD_LPM_ENABLED == 1) || (USBD_CLASS_BOS_ENABLED == 1))
uint8_t *USBD_USR_BOSDescriptor(USBD_SpeedTypeDef speed , uint16_t *length);
#endif

/* Private variables ---------------------------------------------------------*/
USBD_DescriptorsTypeDef USB1_Desc =
{
  USBD_Class_DeviceDescriptor,
  USBD_Class_LangIDStrDescriptor,
  USBD_Class_ManufacturerStrDescriptor,
  USBD_Class_ProductStrDescriptor,
  USBD_Class_SerialStrDescriptor,
  USBD_Class_ConfigStrDescriptor,
  USBD_Class_InterfaceStrDescriptor,
#if (USBD_CLASS_USER_STRING_DESC == 1)
  USBD_Class_UserStrDescriptor,
#endif

#if ((USBD_LPM_ENABLED == 1) || (USBD_CLASS_BOS_ENABLED == 1))
  USBD_USR_BOSDescriptor,
#endif
};

/* USB Standard Device Descriptor */
__ALIGN_BEGIN uint8_t USBD_DeviceDesc[USB_LEN_DEV_DESC] __ALIGN_END =
{
  0x12,                                                            /* bLength */
  USB_DESC_TYPE_DEVICE,                                            /* bDescriptorType */
#if ((USBD_LPM_ENABLED == 1) || (USBD_CLASS_BOS_ENABLED == 1))
  cLittleTo2Bytes(0x0201U),                                        /*bcdUSB */     /* changed to USB version 2.01 in order to support BOS Desc */
#else
  cLittleTo2Bytes(0x0200U),                                        /* bcdUSB */
#endif
  0xEF,                                                            /* bDeviceClass */
  0x02,                                                            /* bDeviceSubClass */
  0x01,                                                            /* bDeviceProtocol */
  USB_MAX_EP0_SIZE,                                                /* bMaxPacketSize */
  cLittleTo2Bytes(USBD_VID),                                       /* idVendor */
  cLittleTo2Bytes(USBD_PID),                                       /* idVendor */
  cLittleTo2Bytes(0x0200U),                                        /* bcdDevice rel. 2.00 */
  USBD_IDX_MFC_STR,                                                /* Index of manufacturer string */
  USBD_IDX_PRODUCT_STR,                                            /* Index of product string */
  USBD_IDX_SERIAL_STR,                                             /* Index of serial number string */
  USBD_MAX_NUM_CONFIGURATION                                       /* bNumConfigurations */
}; /* USB_DeviceDescriptor */






#define MS_OS_20_DescriptorSet_Size (166U)
__align(4) const unsigned char MS_OS_20_DescriptorSet[MS_OS_20_DescriptorSet_Size]={

  // **************** Microsoft OS 2.0 Descriptor Set Header ****************
  cLittleTo2Bytes(10U),                                            /* wLength */
  cLittleTo2Bytes(MS_OS_20_SET_HEADER_DESCRIPTOR),                 /* wDescriptorType */
  cLittleTo4Bytes(0x06030000U),                                    /* dwWindowsVersion: 0x06030000 for Windows Blue */
  cLittleTo2Bytes(MS_OS_20_DescriptorSet_Size),                    /* wTotalLength */

  // **************** Microsoft OS 2.0 configuration subset header ****************
  // cLittleTo2Bytes(8U),                                          /* wLength */
  // cLittleTo2Bytes(MS_OS_20_SUBSET_HEADER_CONFIGURATION),        /* wDescriptorType */
  // 0x00U,                                                        /* bConfigurationValue 如果启用此段 这个必须要为0(实际测试得出) 但USB只有配置1在工作 不知道是微软驱动的bug还是我有那些疏忽了 */
  // 0x00U,                                                        /* bReserved */
  // cLittleTo2Bytes(164U),                                        /* wTotalLength */

  // **************** Microsoft OS 2.0 function subset header ****************
  cLittleTo2Bytes(8U),                                             /* wLength */
  cLittleTo2Bytes(MS_OS_20_SUBSET_HEADER_FUNCTION),                /* wDescriptorType */
  0x00U,                                                           /* bFirstInterface 该方法对应的第一个接口号 */
  0x00U,                                                           /* bReserved */
  cLittleTo2Bytes(156U),                                           /* wSubsetLength */

  // **************** Microsoft OS 2.0 compatible ID descriptor ****************
  cLittleTo2Bytes(20U),                                            /* wLength */
  cLittleTo2Bytes(MS_OS_20_FEATURE_COMPATIBLE_ID),                 /* wDescriptorType */
  'W','I','N','U','S','B',0x00U,0x00U,                             /* CompatibleID */
  0x00U,0x00U,0x00U,0x00U,0x00U,0x00U,0x00U,0x00U,                 /* SubCompatibleID */

  // **************** Microsoft OS 2.0 registry property descriptor ****************
  cLittleTo2Bytes(128U),                                        /* wLength */
  cLittleTo2Bytes(MS_OS_20_FEATURE_REG_PROPERTY),               /* wDescriptorType */
  cLittleTo2Bytes(MS_OS_20_REG_PROPERTY_PropertyDataType_REG_SZ),  /* wPropertyDataType */
  cLittleTo2Bytes(40U),                                         /* wPropertyNameLength */
  'D',0x00U,
  'e',0x00U,
  'v',0x00U,
  'i',0x00U,
  'c',0x00U,
  'e',0x00U,
  'I',0x00U,
  'n',0x00U,
  't',0x00U,
  'e',0x00U,
  'r',0x00U,
  'f',0x00U,
  'a',0x00U,
  'c',0x00U,
  'e',0x00U,
  'G',0x00U,
  'U',0x00U,
  'I',0x00U,
  'D',0x00U,
  0x00U,0x00U,                                                     /* PropertyName: "DeviceInterfaceGUID" */
  cLittleTo2Bytes(78U),                                            /* wPropertyDataLength */
  '{',0x00U,
  'C',0x00U,
  'D',0x00U,
  'B',0x00U,
  '3',0x00U,
  'B',0x00U,
  '5',0x00U,
  'A',0x00U,
  'D',0x00U,
  '-',0x00U,
  '2',0x00U,
  '9',0x00U,
  '3',0x00U,
  'B',0x00U,
  '-',0x00U,
  '4',0x00U,
  '6',0x00U,
  '6',0x00U,
  '3',0x00U,
  '-',0x00U,
  'A',0x00U,
  'A',0x00U,
  '3',0x00U,
  '6',0x00U,
  '-',0x00U,
  '1',0x00U,
  'A',0x00U,
  'A',0x00U,
  'E',0x00U,
  '4',0x00U,
  '6',0x00U,
  '4',0x00U,
  '6',0x00U,
  '3',0x00U,
  '7',0x00U,
  '7',0x00U,
  '6',0x00U,
  '}',0x00U,
  0x00U,0x00U,                                                     /* PropertyData: "{CDB3B5AD-293B-4663-AA36-1AAE46463776}" */

};

#define USBD_BOSDesc_Size          33U
__ALIGN_BEGIN const uint8_t USBD_BOSDesc[USBD_BOSDesc_Size] __ALIGN_END =
{
  0x05U,                                                           /* bLength */
  USB_DESC_TYPE_BOS,                                               /* Device Descriptor Type */
  cLittleTo2Bytes(USBD_BOSDesc_Size),                              /* Total length of BOS descriptor and all of its sub descs */
  0x01U,                                                           /* The number of separate device capability descriptors in the BOS */

  // **************** Device Capability Descriptor: PLATFORM ****************
  28U,                                                             /* bLength */
  USB_DEVICE_CAPABITY_TYPE,                                        /* bDescriptorType: DEVICE CAPABILITY Type */
  0x05U,                                                           /* bDevCapabilityType: 0x05:PLATFORM */
  0x00U,                                                           /* bReserved */
  0xDFU,0x60U,0xDDU,0xD8U,                                         /* PlatformCapabilityUUID: D8DD60DF-4589-4CC7-9CD2-659D9E648A9F */
  0x89U,0x45U,0xC7U,0x4CU,
  0x9CU,0xD2U,0x65U,0x9DU,                                         //突然变大端模式了?? 还是这一节代表的就是数组?
  0x9EU,0x64U,0x8AU,0x9FU,                                         //突然变大端模式了?? 还是这一节代表的就是数组?
  cLittleTo4Bytes(0x06030000U),                                    /* dwWindowsVersion: 0x06030000 for Windows Blue */
  cLittleTo2Bytes(MS_OS_20_DescriptorSet_Size),                    /* wMSOSDescriptorSetTotalLength */
  USB_MS_VendorCode,                                               /* bMS_VendorCode */
  0x00U,                                                           /* bAltEnumCode */

};


/* USB Standard Device Descriptor */
#if defined ( __ICCARM__ ) /*!< IAR Compiler */
#pragma data_alignment=4
#endif
__ALIGN_BEGIN uint8_t USBD_LangIDDesc[USB_LEN_LANGID_STR_DESC] __ALIGN_END =
{
  USB_LEN_LANGID_STR_DESC,
  USB_DESC_TYPE_STRING,
  LOBYTE(USBD_LANGID_STRING),
  HIBYTE(USBD_LANGID_STRING),
};

__ALIGN_BEGIN uint8_t USBD_StringSerial[USB_SIZ_STRING_SERIAL] =
{
  USB_SIZ_STRING_SERIAL,
  USB_DESC_TYPE_STRING,
};


//字符串:F4_SWD_Devices
__align(4) const static unsigned char USBD_ProductStrDescriptor[]={
   0x1EU,0x03U,0x46U,0x00U,0x34U,0x00U,0x5FU,0x00U
  ,0x53U,0x00U,0x57U,0x00U,0x44U,0x00U,0x5FU,0x00U
  ,0x44U,0x00U,0x65U,0x00U,0x76U,0x00U,0x69U,0x00U
  ,0x63U,0x00U,0x65U,0x00U,0x73U,0x00U
};

//字符串:虚拟串口-0x9DEFA478
__align(4) const static unsigned char USBD_UserStrDescriptor_0[]={
   0x20U,0x03U,0x5AU,0x86U,0xDFU,0x62U,0x32U,0x4EU
  ,0xE3U,0x53U,0x2DU,0x00U,0x30U,0x00U,0x78U,0x00U
  ,0x39U,0x00U,0x44U,0x00U,0x45U,0x00U,0x46U,0x00U
  ,0x41U,0x00U,0x34U,0x00U,0x37U,0x00U,0x38U,0x00U
};



__ALIGN_BEGIN uint8_t USBD_StrDesc[USBD_MAX_STR_DESC_SIZ] __ALIGN_END;

/* Private functions ---------------------------------------------------------*/
static void IntToUnicode(uint32_t value, uint8_t *pbuf, uint8_t len);
static void Get_SerialNum(void);

/**
 * @brief 获取 MS_OS_20_DescriptorSet
 * @param speed Current device speed
 * @param length Pointer to data length variable
 * @return Pointer to descriptor buffer
 */
unsigned char* USBD_Class_MS_OS_20_DescriptorSet(USBD_SpeedTypeDef speed,unsigned int* length){

  *length = MS_OS_20_DescriptorSet_Size;
  return (uint8_t *)MS_OS_20_DescriptorSet;
}

/**
  * @brief  Returns the device descriptor.
  * @param  speed: Current device speed
  * @param  length: Pointer to data length variable
  * @retval Pointer to descriptor buffer
  */
uint8_t *USBD_Class_DeviceDescriptor(USBD_SpeedTypeDef speed, uint16_t *length)
{

  *length = USB_LEN_DEV_DESC;
  return (uint8_t *)USBD_DeviceDesc;
}

/**
  * @brief  Returns the LangID string descriptor.
  * @param  speed: Current device speed
  * @param  length: Pointer to data length variable
  * @retval Pointer to descriptor buffer
  */
uint8_t *USBD_Class_LangIDStrDescriptor(USBD_SpeedTypeDef speed, uint16_t *length)
{

  *length = USB_LEN_LANGID_STR_DESC;
  return (uint8_t *)USBD_LangIDDesc;
}

/**
  * @brief  Returns the product string descriptor.
  * @param  speed: Current device speed
  * @param  length: Pointer to data length variable
  * @retval Pointer to descriptor buffer
  */
uint8_t *USBD_Class_ProductStrDescriptor(USBD_SpeedTypeDef speed, uint16_t *length)
{
  
  *length=USBD_ProductStrDescriptor[0];

  return (unsigned char*)USBD_ProductStrDescriptor;
}

/**
  * @brief  Returns the manufacturer string descriptor.
  * @param  speed: Current device speed
  * @param  length: Pointer to data length variable
  * @retval Pointer to descriptor buffer
  */
uint8_t *USBD_Class_ManufacturerStrDescriptor(USBD_SpeedTypeDef speed, uint16_t *length)
{

  USBD_GetString((uint8_t *)USBD_MANUFACTURER_STRING, USBD_StrDesc, length);
  return USBD_StrDesc;
}

/**
  * @brief  Returns the serial number string descriptor.
  * @param  speed: Current device speed
  * @param  length: Pointer to data length variable
  * @retval Pointer to descriptor buffer
  */
uint8_t *USBD_Class_SerialStrDescriptor(USBD_SpeedTypeDef speed, uint16_t *length)
{

  *length = USB_SIZ_STRING_SERIAL;

  /* Update the serial number string descriptor with the data from the unique ID*/
  Get_SerialNum();

  return (uint8_t *)USBD_StringSerial;
}

/**
  * @brief  Returns the configuration string descriptor.
  * @param  speed: Current device speed
  * @param  length: Pointer to data length variable
  * @retval Pointer to descriptor buffer
  */
uint8_t *USBD_Class_ConfigStrDescriptor(USBD_SpeedTypeDef speed, uint16_t *length)
{
  if (speed == USBD_SPEED_HIGH)
  {
    USBD_GetString((uint8_t *)USBD_CONFIGURATION_HS_STRING, USBD_StrDesc, length);
  }
  else
  {
    USBD_GetString((uint8_t *)USBD_CONFIGURATION_FS_STRING, USBD_StrDesc, length);
  }
  return USBD_StrDesc;
}

/**
  * @brief  Returns the interface string descriptor.
  * @param  speed: Current device speed
  * @param  length: Pointer to data length variable
  * @retval Pointer to descriptor buffer
  */
uint8_t *USBD_Class_InterfaceStrDescriptor(USBD_SpeedTypeDef speed, uint16_t *length)
{
  if (speed == USBD_SPEED_HIGH)
  {
    USBD_GetString((uint8_t *)USBD_INTERFACE_HS_STRING, USBD_StrDesc, length);
  }
  else
  {
    USBD_GetString((uint8_t *)USBD_INTERFACE_FS_STRING, USBD_StrDesc, length);
  }
  return USBD_StrDesc;
}

/**
  * @brief  Create the serial number string descriptor
  * @param  None
  * @retval None
  */
static void Get_SerialNum(void)
{
  uint32_t deviceserial0, deviceserial1, deviceserial2;

  deviceserial0 = *(uint32_t *)DEVICE_ID1;
  deviceserial1 = *(uint32_t *)DEVICE_ID2;
  deviceserial2 = *(uint32_t *)DEVICE_ID3;

  deviceserial0 += deviceserial2;

  if (deviceserial0 != 0U)
  {
    IntToUnicode(deviceserial0, &USBD_StringSerial[2], 8U);
    IntToUnicode(deviceserial1, &USBD_StringSerial[18], 4U);
  }
}


#if ((USBD_LPM_ENABLED == 1) || (USBD_CLASS_BOS_ENABLED == 1))
/**
  * @brief  USBD_USR_BOSDescriptor
  *         return the BOS descriptor
  * @param  speed : current device speed
  * @param  length : pointer to data length variable
  * @retval pointer to descriptor buffer
  */
uint8_t *USBD_USR_BOSDescriptor(USBD_SpeedTypeDef speed , uint16_t *length)
{
  *length = USBD_BOSDesc_Size;
  return (uint8_t*)USBD_BOSDesc;
}
#endif


#if (USBD_CLASS_USER_STRING_DESC == 1)
/**
  * @brief  Returns the Class User string descriptor.
  * @param  speed: Current device speed
  * @param  idx: index of string descriptor
  * @param  length: Pointer to data length variable
  * @retval Pointer to descriptor buffer
  */
uint8_t *USBD_Class_UserStrDescriptor(USBD_SpeedTypeDef speed, uint8_t idx, uint16_t *length)
{

  switch(idx){
    case 0x67U:
      *length=USBD_UserStrDescriptor_0[0];
      return (unsigned char*)USBD_UserStrDescriptor_0;
    default:
      break;
  }

  *length=USBD_ProductStrDescriptor[0];
  return (unsigned char*)USBD_ProductStrDescriptor;
}
#endif


/**
  * @brief  Convert Hex 32Bits value into char
  * @param  value: value to convert
  * @param  pbuf: pointer to the buffer
  * @param  len: buffer length
  * @retval None
  */
static void IntToUnicode(uint32_t value, uint8_t *pbuf, uint8_t len)
{
  uint8_t idx = 0U;

  for (idx = 0U ; idx < len ; idx ++)
  {
    if (((value >> 28)) < 0xAU)
    {
      pbuf[ 2U * idx] = (value >> 28) + '0';
    }
    else
    {
      pbuf[2U * idx] = (value >> 28) + 'A' - 10U;
    }

    value = value << 4;

    pbuf[2U * idx + 1] = 0U;
  }
}

