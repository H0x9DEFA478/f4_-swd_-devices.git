/*
 * @Author: 0x9DEFA478
 * @Date: 2021-08-08 16:39:20
 * @LastEditTime: 2021-08-11 15:42:24
 * @LastEditors: 0x9DEFA478
 * @Description: 
 * QQ:2652450237
 * ============================================================================================================================================
 * 
 * 
 * 
 *                                                                                               ************      ****************************
 *                                                                                             ************      ****************************  
 *                                                                                           ************      ****************************    
 *                                                                                         ************      ****************************      
 *                                                                                       ************      ************                        
 *                                                                                     ************      ************                          
 *                                                                                   ************      ************                            
 *                                                                                 ************      ************                              
 *                                                                               ************      ************                                
 *                                                                             ************      ************                                  
 *                                                                           ************      ************                                    
 *                                                                         ************      ************                                      
 *                                                                       ************      ************                                        
 *                                                                     ************      ************                                          
 *                                                                   ************      ************                                            
 *                                                                 ************      ************                                              
 *                                                               ************      ************                                                
 *                                                             ************      ************                                                  
 *                                                           ************      ************                                                    
 *                                                         ************      ************                                                      
 *                                                       ************      ************                                                        
 *                                                     ************      ************                                                          
 *                                                   ************      ************                                                            
 *                                                 ************      ************                                                              
 *                                               ************      ************                                                                
 *                                             ************      ************                                                                  
 *                                           ************      ************                                                                    
 *                                         ************      ************                                                                      
 *                                       ************      ************                                                                        
 *                                     ************      ************                                                                          
 *                                   ************      ************                                                                            
 *                                 ************      ************                                                                              
 *                               ************      ************                                                                                
 *                             ************      ************                                                                                  
 *                           ************      ************                                                                                    
 *                         ************      ************                                                                                      
 *       ****************************      ************                                                                                        
 *     ****************************      ************                                                                                          
 *   ****************************      ************                                                                                            
 * ****************************      ************                                                                                              
 * 
 * 
 * 
 * ============================================================================================================================================
 * 
 */
#ifndef __H_USB_Desc_H_
#define __H_USB_Desc_H_


#define  vH_USB_DescType_Device                         0x01U
#define  vH_USB_DescType_Configuration                  0x02U
#define  vH_USB_DescType_String                         0x03U
#define  vH_USB_DescType_Interface                      0x04U
#define  vH_USB_DescType_Endpoint                       0x05U
#define  vH_USB_DescType_DeviceQualifier                0x06U
#define  vH_USB_DescType_OtherSpeedConfiguration        0x07U
#define  vH_USB_DescType_InterfaceAssociation           0x0BU
#define  vH_USB_DescType_BOS                            0x0FU



#define cLittleTo2Bytes(value)                          ((value)&0xFFU),(((value)>>8)&0xFFU)
#define cLittleTo4Bytes(value)                          ((value)&0xFFU),(((value)>>8)&0xFFU),(((value)>>16)&0xFFU),(((value)>>24)&0xFFU)


//============================================================================================================================================
//winusb
//============================================================================================================================================

#define MS_OS_20_SET_HEADER_DESCRIPTOR        0x00U
#define MS_OS_20_SUBSET_HEADER_CONFIGURATION  0x01U
#define MS_OS_20_SUBSET_HEADER_FUNCTION       0x02U
#define MS_OS_20_FEATURE_COMPATIBLE_ID        0x03U
#define MS_OS_20_FEATURE_REG_PROPERTY         0x04U
#define MS_OS_20_FEATURE_MIN_RESUME_TIME      0x05U
#define MS_OS_20_FEATURE_MODEL_ID             0x06U
#define MS_OS_20_FEATURE_CCGP_DEVICE          0x07U
#define MS_OS_20_FEATURE_VENDOR_REVISION      0x08U

#define MS_OS_20_REG_PROPERTY_PropertyDataType_RESERVED                 0x00U
#define MS_OS_20_REG_PROPERTY_PropertyDataType_REG_SZ                   0x01U
#define MS_OS_20_REG_PROPERTY_PropertyDataType_REG_EXPAND_SZ            0x02U
#define MS_OS_20_REG_PROPERTY_PropertyDataType_REG_BINARY               0x03U
#define MS_OS_20_REG_PROPERTY_PropertyDataType_REG_DWORD_LITTLE_ENDIAN  0x04U
#define MS_OS_20_REG_PROPERTY_PropertyDataType_REG_DWORD_BIG_ENDIAN     0x05U
#define MS_OS_20_REG_PROPERTY_PropertyDataType_REG_LINK                 0x06U
#define MS_OS_20_REG_PROPERTY_PropertyDataType_REG_MULTI_SZ             0x07U







#endif //__H_USB_Desc_H_
