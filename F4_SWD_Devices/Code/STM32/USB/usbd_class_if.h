/*
 * @Author: 0x9DEFA478
 * @Date: 2021-08-07 20:08:03
 * @LastEditTime: 2021-08-16 00:46:14
 * @LastEditors: 0x9DEFA478
 * @Description: 
 * QQ:2652450237
 * ============================================================================================================================================
 * 
 * 
 * 
 *                                                                                               ************      ****************************
 *                                                                                             ************      ****************************  
 *                                                                                           ************      ****************************    
 *                                                                                         ************      ****************************      
 *                                                                                       ************      ************                        
 *                                                                                     ************      ************                          
 *                                                                                   ************      ************                            
 *                                                                                 ************      ************                              
 *                                                                               ************      ************                                
 *                                                                             ************      ************                                  
 *                                                                           ************      ************                                    
 *                                                                         ************      ************                                      
 *                                                                       ************      ************                                        
 *                                                                     ************      ************                                          
 *                                                                   ************      ************                                            
 *                                                                 ************      ************                                              
 *                                                               ************      ************                                                
 *                                                             ************      ************                                                  
 *                                                           ************      ************                                                    
 *                                                         ************      ************                                                      
 *                                                       ************      ************                                                        
 *                                                     ************      ************                                                          
 *                                                   ************      ************                                                            
 *                                                 ************      ************                                                              
 *                                               ************      ************                                                                
 *                                             ************      ************                                                                  
 *                                           ************      ************                                                                    
 *                                         ************      ************                                                                      
 *                                       ************      ************                                                                        
 *                                     ************      ************                                                                          
 *                                   ************      ************                                                                            
 *                                 ************      ************                                                                              
 *                               ************      ************                                                                                
 *                             ************      ************                                                                                  
 *                           ************      ************                                                                                    
 *                         ************      ************                                                                                      
 *       ****************************      ************                                                                                        
 *     ****************************      ************                                                                                          
 *   ****************************      ************                                                                                            
 * ****************************      ************                                                                                              
 * 
 * 
 * 
 * ============================================================================================================================================
 * 
 */
#ifndef __usbd_class_if_H_
#define __usbd_class_if_H_


/**
 * @brief usb连接回调
 * @param CfgId 配置号
 * @return 无
 */
void usbd_class_ConnectCallback(int CfgId);

/**
 * @brief usb断开连接回调
 * @param CfgId 配置号
 * @return 无
 */
void usbd_class_DisConnectCallback(int CfgId);

/**
 * @brief Winusb_CMSIS_DAP接收回调 被USB中断调用
 * @param Data 接收到的数据
 * @param Length 接收到的数据大小
 * @return 无
 */
void usbd_class_Winusb_CMSIS_DAP_RecvCallback(unsigned char* Data,int Length);

/**
 * @brief Winusb_CMSIS_DAP发送完成回调 被USB中断调用
 * @return 无
 */
void usbd_class_Winusb_CMSIS_DAP_SendDoneCallback(void);

/**
 * @brief CDC接收回调 被USB中断调用
 * @param Data 接收到的数据
 * @param Length 接收到的数据大小
 * @return 无
 */
void usbd_class_CDC_RecvCallback(unsigned char* Data,int Length);

/**
 * @brief CDC发送完成回调 被USB中断调用
 * @return 无
 */
void usbd_class_CDC_SendDoneCallback(void);

/**
 * @brief CDC请求回调
 * @param cmd 请求 bRequest
 * @param data 数据
 * @param Length 数据长度
 * @return 无
 */
void usbd_class_CDC_Ctrl_Callback(unsigned char cmd,unsigned char* data,int Length);





/**
 * @brief usb是否连接
 * @return 0: 无连接 其他: 连接
 */
int usbd_class_IsConnect(void);







/**
 * @brief 发送数据 
 * @param Data 要发送的数据 此地址要4字节对齐
 * @param Length 发送的数据长度
 * @return 0:成功 其他:失败
 */
int usbd_class_Winusb_CMSIS_DAP_Send(unsigned char* Data,int Length);

/**
 * @brief 检查发送是否忙碌
 * @return 0:空闲 其他:忙碌
 */
int usbd_class_Winusb_CMSIS_DAP_IsBusy(void);

/**
 * @brief 发送数据 
 * @param Data 要发送的数据 此地址要4字节对齐
 * @param Length 发送的数据长度
 * @return 0:成功 其他:失败
 */
int usbd_class_CDC_Send(unsigned char* Data,int Length);

/**
 * @brief 检查发送是否忙碌
 * @return 0:空闲 其他:忙碌
 */
int usbd_class_CDC_IsBusy(void);


#endif //__usbd_class_if_H_
