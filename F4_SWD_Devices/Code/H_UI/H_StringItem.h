/*
 * @Author: 0x9DEFA478
 * @Date: 2021-07-18 20:42:41
 * @LastEditTime: 2021-07-24 23:56:40
 * @LastEditors: 0x9DEFA478
 * @Description: 
 * QQ:2652450237
 * ============================================================================================================================================
 * 
 * 
 * 
 *                                                                                               ************      ****************************
 *                                                                                             ************      ****************************  
 *                                                                                           ************      ****************************    
 *                                                                                         ************      ****************************      
 *                                                                                       ************      ************                        
 *                                                                                     ************      ************                          
 *                                                                                   ************      ************                            
 *                                                                                 ************      ************                              
 *                                                                               ************      ************                                
 *                                                                             ************      ************                                  
 *                                                                           ************      ************                                    
 *                                                                         ************      ************                                      
 *                                                                       ************      ************                                        
 *                                                                     ************      ************                                          
 *                                                                   ************      ************                                            
 *                                                                 ************      ************                                              
 *                                                               ************      ************                                                
 *                                                             ************      ************                                                  
 *                                                           ************      ************                                                    
 *                                                         ************      ************                                                      
 *                                                       ************      ************                                                        
 *                                                     ************      ************                                                          
 *                                                   ************      ************                                                            
 *                                                 ************      ************                                                              
 *                                               ************      ************                                                                
 *                                             ************      ************                                                                  
 *                                           ************      ************                                                                    
 *                                         ************      ************                                                                      
 *                                       ************      ************                                                                        
 *                                     ************      ************                                                                          
 *                                   ************      ************                                                                            
 *                                 ************      ************                                                                              
 *                               ************      ************                                                                                
 *                             ************      ************                                                                                  
 *                           ************      ************                                                                                    
 *                         ************      ************                                                                                      
 *       ****************************      ************                                                                                        
 *     ****************************      ************                                                                                          
 *   ****************************      ************                                                                                            
 * ****************************      ************                                                                                              
 * 
 * 
 * 
 * ============================================================================================================================================
 * 
 */
#ifndef __H_StringItem_H_
#define __H_StringItem_H_
#include "H_Type.h"


#define vH_StringItem_Status_Default   0
#define vH_StringItem_Status_HighLight 1
#define vH_StringItem_Status_Select    2

typedef struct _H_StringItem_ListItem{
  void* Parent;

  char* String;
  int StringMemSize;
  int Height;//选项高度
  void* Font;//字体对象

  void (*AddClickCallback)(struct _H_StringItem_ListItem*);//增加事件回调 可直接修改此段
  void (*SubClickCallback)(struct _H_StringItem_ListItem*);//减少事件回调 可直接修改此段
  void (*SelectCallback)(struct _H_StringItem_ListItem*);//选择事件回调 可直接修改此段
  void (*DeSelectCallback)(struct _H_StringItem_ListItem*);//取消选择事件回调 可直接修改此段

  struct{
    struct _H_StringItem_ListItem* Last;
    struct _H_StringItem_ListItem* Next;
  }List;
  

}H_StringItem_ListItem;

typedef struct _H_StringItem_List{
  
  int X;//坐标X
  int Y;//坐标Y
  int Width;//宽度
  int Height;//高度

  H_StringItem_ListItem* Items;//正在显示的开头选项
  int isSelect;//高亮的选项是否被选择
  H_StringItem_ListItem* HighLightItem;//高亮的选项

  void* (*Malloc)(Husize);
  void (*Free)(void*);
  void (*ExitCallback)(struct _H_StringItem_List*);//返回事件回调 可直接修改此段

}H_StringItem_List;


typedef struct{
  

  void* (*Malloc)(Husize);
  void (*Free)(void*);

  void (*DrawString)(char* String,void* Font,int Status,int X,int Y,int MaxX,int MaxY);

  
  H_StringItem_List* NowList;//正在显示的选项链表

}H_StringItem;






/**
 * @brief 新建一个 H_StringItem
 * @param GetFontHeight 获取字体高度方法
 * @param DrawString 绘制字符串方法
 * @param Malloc 获取内存方法
 * @param Free 释放内存方法
 * @return H_StringItem 指针
 */
H_StringItem* new_H_StringItem(void (*DrawString)(char* String,void* Font,int Status,int X,int Y,int MaxX,int MaxY)
  ,void* (*Malloc)(Husize),void (*Free)(void*));

/**
 * @brief 新建一个链表
 * @param StringItem H_StringItem对象
 * @param X X坐标
 * @param Y Y坐标
 * @param Width 宽度
 * @param Height 高度
 * @return H_StringItem_List 对象
 */
H_StringItem_List* new_H_StringItem_List(H_StringItem* StringItem,int X,int Y,int Width,int Height);

/**
 * @brief 添加列表项
 * @param StringItem_List 要添加到的列表
 * @param Height 项高度 要小于等于列表(H_StringItem_List)的高度
 * @return H_StringItem_ListItem对象
 */
H_StringItem_ListItem* H_StringItem_List_AddItem(H_StringItem_List* StringItem_List,int Height);

/**
 * @brief 删除H_StringItem (仅删除StringItem本身)
 * @param StringItem 要删除的对象
 * @return 无
 */
void delete_H_StringItem(H_StringItem* StringItem);

/**
 * @brief 删除列表 会将其中的项删除 在StringItem_List从移除之后才能使用此方法
 * @param StringItem_List 要删除的列表
 * @return 无
 */
void delete_H_StringItem_List(H_StringItem_List* StringItem_List);

/**
 * @brief 设置H_StringItem正在使用的列表
 * @param StringItem 要设置的H_StringItem
 * @param List 需要设置成的列表
 * @return 无
 */
void H_StringItem_SetList(H_StringItem* StringItem,H_StringItem_List* List);

/**
 * @brief 设置项的字符串
 * @param Item 要设置的项
 * @param String 字符串
 * @return noEdit
 */
void H_StringItem_SetString(H_StringItem_ListItem* Item,char* String);

/**
 * @brief 设置项的字体
 * @param Item 要设置的项
 * @param Font 字体
 * @return 无
 */
void H_StringItem_SetFont(H_StringItem_ListItem* Item,void* Font);

/**
 * @brief 发生确认事件时 调用此方法
 * @param StringItem H_StringItem
 * @return 无
 */
void H_StringItem_EventEnter(H_StringItem* StringItem);

/**
 * @brief 发生返回事件时 调用此方法
 * @param StringItem H_StringItem
 * @return 无
 */
void H_StringItem_EventBack(H_StringItem* StringItem);

/**
 * @brief 发生增加事件时 调用此方法
 * @param StringItem H_StringItem
 * @return 无
 */
void H_StringItem_EventAdd(H_StringItem* StringItem);

/**
 * @brief 发生减少事件时 调用此方法
 * @param StringItem H_StringItem
 * @return 无
 */
void H_StringItem_EventSub(H_StringItem* StringItem);

/**
 * @brief 绘制字符串
 * @param StringItem StringItem对象
 * @return 无
 */
void H_StringItem_Draw(H_StringItem* StringItem);

#endif //__H_StringItem_H_
