/*
 * @Author: 0x9DEFA478
 * @Date: 2021-07-31 17:05:02
 * @LastEditTime: 2021-08-01 16:43:16
 * @LastEditors: 0x9DEFA478
 * @Description: 
 * QQ:2652450237
 * ============================================================================================================================================
 * 
 * 
 * 
 *                                                                                               ************      ****************************
 *                                                                                             ************      ****************************  
 *                                                                                           ************      ****************************    
 *                                                                                         ************      ****************************      
 *                                                                                       ************      ************                        
 *                                                                                     ************      ************                          
 *                                                                                   ************      ************                            
 *                                                                                 ************      ************                              
 *                                                                               ************      ************                                
 *                                                                             ************      ************                                  
 *                                                                           ************      ************                                    
 *                                                                         ************      ************                                      
 *                                                                       ************      ************                                        
 *                                                                     ************      ************                                          
 *                                                                   ************      ************                                            
 *                                                                 ************      ************                                              
 *                                                               ************      ************                                                
 *                                                             ************      ************                                                  
 *                                                           ************      ************                                                    
 *                                                         ************      ************                                                      
 *                                                       ************      ************                                                        
 *                                                     ************      ************                                                          
 *                                                   ************      ************                                                            
 *                                                 ************      ************                                                              
 *                                               ************      ************                                                                
 *                                             ************      ************                                                                  
 *                                           ************      ************                                                                    
 *                                         ************      ************                                                                      
 *                                       ************      ************                                                                        
 *                                     ************      ************                                                                          
 *                                   ************      ************                                                                            
 *                                 ************      ************                                                                              
 *                               ************      ************                                                                                
 *                             ************      ************                                                                                  
 *                           ************      ************                                                                                    
 *                         ************      ************                                                                                      
 *       ****************************      ************                                                                                        
 *     ****************************      ************                                                                                          
 *   ****************************      ************                                                                                            
 * ****************************      ************                                                                                              
 * 
 * 
 * 
 * ============================================================================================================================================
 * 
 */
#ifndef __H_UI_H_
#define __H_UI_H_
#include "H_Type.h"


typedef struct{
  //进入页面回调
  void (*EnterPageCallback)(void*);
  //进入退出回调
  void (*ExitPageCallback)(void*);
  //显示回调
  void (*DisplayPageCallback)(void*);
}H_UI_Callbacks;

typedef struct _H_UI_Page{
  void* Parent;

  H_UI_Callbacks Callbacks;

  struct{
    struct _H_UI_Page* Next;
    struct _H_UI_Page* Last;
  }List;

  //H_UI并没有使用此段 用户可自由使用此段
  void* UserInstence;

}H_UI_Page;

typedef struct _H_UI{
  

  //当前的页面
  H_UI_Page* Page;

  H_UI_Page* EmptyPage;//PageList的空节点

  struct{
    void* (*Malloc)(Husize);
    void (*Free)(void*);
  }Memory;

}H_UI;







/**
 * @brief 获得一个新的H_UI
 * @param Malloc 内存申请方法
 * @param Free 内存释放方法
 * @return 新的H_UI 如果为NULL 则为失败
 */
H_UI* new_H_UI(void* (*Malloc)(Husize),void (*Free)(void*));

/**
 * @brief 删除 H_UI 会将其下的节点删除
 * @param h_ui 要删除的H_UI
 * @return 无
 */
void delete_H_UI(H_UI* h_ui);

/**
 * @brief 添加H_UI新页面 添加到参考页面对象之后
 * @param h_ui H_UI对象
 * @param ref 参考页面
 * @return 新的页面对象 如果为NULL 则新建失败
 */
H_UI_Page* H_UI_NewPage(H_UI* h_ui,H_UI_Page* ref,H_UI_Callbacks* Callbacks);

/**
 * @brief 删除H_UI_Page页面
 * @param h_ui_page 要删除的页面
 * @return 无
 */
void H_UI_DeletePage(H_UI_Page* h_ui_page);

/**
 * @brief 获取H_UI的空页面(空页面不参与显示,仅用于占位置)
 * @param h_ui 要获取的对象
 * @return 空页面
 */
H_UI_Page* H_UI_GetEmptyPage(H_UI* h_ui);

/**
 * @brief 切换到下一个页面
 * @param h_ui 操作的h_ui
 * @return 无
 */
void H_UI_Page_NextPage(H_UI* h_ui);

/**
 * @brief 切换到上一个页面
 * @param h_ui 操作的h_ui
 * @return 无
 */
void H_UI_Page_LastPage(H_UI* h_ui);

/**
 * @brief 调用正在显示页面的显示方法
 * @param h_ui 操作的h_ui
 * @return 无
 */
void H_UI_Page_Display(H_UI* h_ui);

#endif //__H_UI_H_
