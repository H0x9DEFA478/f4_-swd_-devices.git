/*
 * @Author: 0x9DEFA478
 * @Date: 2021-08-13 17:10:26
 * @LastEditTime: 2021-08-16 19:34:33
 * @LastEditors: 0x9DEFA478
 * @Description: 
 * QQ:2652450237
 * ============================================================================================================================================
 * 
 * 
 * 
 *                                                                                               ************      ****************************
 *                                                                                             ************      ****************************  
 *                                                                                           ************      ****************************    
 *                                                                                         ************      ****************************      
 *                                                                                       ************      ************                        
 *                                                                                     ************      ************                          
 *                                                                                   ************      ************                            
 *                                                                                 ************      ************                              
 *                                                                               ************      ************                                
 *                                                                             ************      ************                                  
 *                                                                           ************      ************                                    
 *                                                                         ************      ************                                      
 *                                                                       ************      ************                                        
 *                                                                     ************      ************                                          
 *                                                                   ************      ************                                            
 *                                                                 ************      ************                                              
 *                                                               ************      ************                                                
 *                                                             ************      ************                                                  
 *                                                           ************      ************                                                    
 *                                                         ************      ************                                                      
 *                                                       ************      ************                                                        
 *                                                     ************      ************                                                          
 *                                                   ************      ************                                                            
 *                                                 ************      ************                                                              
 *                                               ************      ************                                                                
 *                                             ************      ************                                                                  
 *                                           ************      ************                                                                    
 *                                         ************      ************                                                                      
 *                                       ************      ************                                                                        
 *                                     ************      ************                                                                          
 *                                   ************      ************                                                                            
 *                                 ************      ************                                                                              
 *                               ************      ************                                                                                
 *                             ************      ************                                                                                  
 *                           ************      ************                                                                                    
 *                         ************      ************                                                                                      
 *       ****************************      ************                                                                                        
 *     ****************************      ************                                                                                          
 *   ****************************      ************                                                                                            
 * ****************************      ************                                                                                              
 * 
 * 
 * 
 * ============================================================================================================================================
 * 
 */
#include "DAP_Thread.h"
#include "DAP_config.h"
#include "DAP.h"
#include "Peripheral.h"


volatile int DAP_IsConnected=0;
volatile int DAP_IsRunning=0;


volatile int DAP_InSpeed=0;
volatile int DAP_OutSpeed=0;

volatile int DAP_IsFastSpeed=0;


#define vDAP_PacketCount    (DAP_PACKET_COUNT+1)
#define vDAP_PacketSizeDiv4 ((int)(DAP_PACKET_SIZE+sizeof(int)-1)/4)
#define vDAP_PacketSize     (vDAP_PacketSizeDiv4*sizeof(int))

typedef struct{

  H_FIFO inFIFO;
  H_FIFO outFIFO;

  H_TS_Semaphore* inSemaphore;
  H_TS_Semaphore* outSemaphore;
  H_TS_Semaphore* transferSemaphore;

  H_TS_Lock* SpeedLock;
  volatile int InCnt;
  volatile int OutCnt;

}DAP_Thread_Stream_Handle;


DAP_Thread_Stream_Handle dap_thread_stream_handle={
  .inSemaphore=NULL,
  .outSemaphore=NULL,
  .transferSemaphore=NULL
};

H_TS_Thread* dap_thread_response_handle;
H_TS_Thread* dap_thread_execute_handle;
H_TS_Thread* dap_thread_speed_handle;


static int inSizeArray[vDAP_PacketCount];
static int inBuffer[vDAP_PacketCount*vDAP_PacketSizeDiv4];
static int outSizeArray[vDAP_PacketCount];
static int outBuffer[vDAP_PacketCount*vDAP_PacketSizeDiv4];
static int ResponsePack[vDAP_PacketSizeDiv4];
static int sendPack[vDAP_PacketSizeDiv4];


/**
 * @brief usb传来的数据使用这个方法传入到DAP
 * @param Data 传入的数据
 * @param Size 数据长度
 * @return 无
 */
void DAP_Thread_InputData(unsigned char* Data,int Size){
  
  
  H_FIFO_Write(&dap_thread_stream_handle.inFIFO,Data,Size);
  H_TS_SemaphoreGive_ISR(dap_thread_stream_handle.inSemaphore);
}

/**
 * @brief usb将数据包发送完成后, 应调用此方法以通知DAP线程有数据包发送完成
 * @return 无
 */
void DAP_Thread_TransferDone(){
  H_TS_SemaphoreGive_ISR(dap_thread_stream_handle.transferSemaphore);
}



int DAP_Thread_Speed(void* v){

  for(;;){
    H_TS_ThreadSleep(500);

    H_TS_LockLock(dap_thread_stream_handle.SpeedLock);

    DAP_InSpeed=dap_thread_stream_handle.InCnt*2;
    DAP_OutSpeed=dap_thread_stream_handle.OutCnt*2;
    dap_thread_stream_handle.OutCnt=0;
    dap_thread_stream_handle.InCnt=0;

    H_TS_LockUnlock(dap_thread_stream_handle.SpeedLock);
  }
}


int DAP_Thread_Response(void* v){
  int readLength;
  unsigned char* readData;
  int i;

  for(;;){
    
    H_TS_SemaphoreTake(dap_thread_stream_handle.outSemaphore);
    readData=H_FIFO_Read(&dap_thread_stream_handle.outFIFO,&readLength);
    if(readData!=NULL){
      
      for(i=0;i<readLength;i++){
        ((unsigned char*)sendPack)[i]=readData[i];
      }

      H_FIFO_Next(&dap_thread_stream_handle.outFIFO);
      
      usbd_class_Winusb_CMSIS_DAP_Send(sendPack,readLength);
      H_TS_SemaphoreTake(dap_thread_stream_handle.transferSemaphore);//阻塞 等待发送完成
      
    }
  }
}

int DAP_Thread_Execute(void* v){
  int readLength;
  unsigned char* readData;
  int Size;

  for(;;){
    
    H_TS_SemaphoreTake(dap_thread_stream_handle.inSemaphore);
    readData=H_FIFO_Read(&dap_thread_stream_handle.inFIFO,&readLength);
    if(readData!=NULL){

      Size=0xFFFF&DAP_ExecuteCommand(readData,(unsigned char*)ResponsePack);
      H_FIFO_Next(&dap_thread_stream_handle.inFIFO);

      H_FIFO_Write(&dap_thread_stream_handle.outFIFO,(unsigned char*)ResponsePack,Size);
      H_TS_SemaphoreGive(dap_thread_stream_handle.outSemaphore);
      H_TS_LockLock(dap_thread_stream_handle.SpeedLock);
      dap_thread_stream_handle.InCnt+=Size;
      dap_thread_stream_handle.OutCnt+=readLength;
      H_TS_LockUnlock(dap_thread_stream_handle.SpeedLock);
    }



  }
}

/**
 * @brief DAP初始化
 * @return 无
 */
void DAP_Thread_Init(){

  DAP_Setup();

  H_FIFO_Init(&dap_thread_stream_handle.inFIFO,vH_FIFO_Type_byte,inBuffer,vDAP_PacketCount*vDAP_PacketSize,inSizeArray,vDAP_PacketCount);
  H_FIFO_Init(&dap_thread_stream_handle.outFIFO,vH_FIFO_Type_byte,outBuffer,vDAP_PacketCount*vDAP_PacketSize,outSizeArray,vDAP_PacketCount);

  dap_thread_stream_handle.inSemaphore=new_H_TS_Semaphore(vDAP_PacketCount);
  dap_thread_stream_handle.outSemaphore=new_H_TS_Semaphore(vDAP_PacketCount);
  dap_thread_stream_handle.transferSemaphore=new_H_TS_Semaphore(vDAP_PacketCount);
  dap_thread_stream_handle.SpeedLock=new_H_TS_Lock();

  
  dap_thread_stream_handle.InCnt=0;
  dap_thread_stream_handle.OutCnt=0;
  DAP_IsFastSpeed=I_NVM.DAP_IsFast;

  //后缀为_LowPriority的新建线程方法，后新建的线程优先级低
  dap_thread_execute_handle=H_TS_StartNewThread_LowPriority(DAP_Thread_Execute,NULL,256,-1024,0);
  dap_thread_response_handle=H_TS_StartNewThread_LowPriority(DAP_Thread_Response,NULL,256,-1024,0);
  dap_thread_speed_handle=H_TS_StartNewThread_LowPriority(DAP_Thread_Speed,NULL,256,-1024,0);


}








