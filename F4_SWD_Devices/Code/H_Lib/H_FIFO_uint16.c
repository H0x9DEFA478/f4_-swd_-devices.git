/*
 * @Author: 0x9DEFA478
 * @Date: 2021-08-14 19:33:36
 * @LastEditTime: 2021-08-14 20:15:50
 * @LastEditors: 0x9DEFA478
 * @Description: 
 * QQ:2652450237
 * ============================================================================================================================================
 * 
 * 
 * 
 *                                                                                               ************      ****************************
 *                                                                                             ************      ****************************  
 *                                                                                           ************      ****************************    
 *                                                                                         ************      ****************************      
 *                                                                                       ************      ************                        
 *                                                                                     ************      ************                          
 *                                                                                   ************      ************                            
 *                                                                                 ************      ************                              
 *                                                                               ************      ************                                
 *                                                                             ************      ************                                  
 *                                                                           ************      ************                                    
 *                                                                         ************      ************                                      
 *                                                                       ************      ************                                        
 *                                                                     ************      ************                                          
 *                                                                   ************      ************                                            
 *                                                                 ************      ************                                              
 *                                                               ************      ************                                                
 *                                                             ************      ************                                                  
 *                                                           ************      ************                                                    
 *                                                         ************      ************                                                      
 *                                                       ************      ************                                                        
 *                                                     ************      ************                                                          
 *                                                   ************      ************                                                            
 *                                                 ************      ************                                                              
 *                                               ************      ************                                                                
 *                                             ************      ************                                                                  
 *                                           ************      ************                                                                    
 *                                         ************      ************                                                                      
 *                                       ************      ************                                                                        
 *                                     ************      ************                                                                          
 *                                   ************      ************                                                                            
 *                                 ************      ************                                                                              
 *                               ************      ************                                                                                
 *                             ************      ************                                                                                  
 *                           ************      ************                                                                                    
 *                         ************      ************                                                                                      
 *       ****************************      ************                                                                                        
 *     ****************************      ************                                                                                          
 *   ****************************      ************                                                                                            
 * ****************************      ************                                                                                              
 * 
 * 
 * 
 * ============================================================================================================================================
 * 
 */
#include "H_FIFO.h"


#define H_FIFO_uint16_Type              Huint16

#define cH_FIFO_uint16_PtrReadByte(ptr) (((H_FIFO_uint16_Type*)(ptr))[0])


int H_FIFO_uint16_Write(H_FIFO* fifo,void* Data,int Length){
  H_FIFO_uint16_Type* src;
  H_FIFO_uint16_Type* dst_ucp;
  int EmptySize;
  int EmptyNum;
  int SizeArray_InId;
  int SizeArray_OutId;
  int Buffer_InId;
  int Buffer_OutId;
  int SizeArraySize;
  int BufferSize;
  int i;

  SizeArray_InId=fifo->SizeArray_InId;
  SizeArray_OutId=fifo->SizeArray_OutId;
  SizeArraySize=fifo->SizeArraySize;
  //判断是否有足够容量
  if(SizeArray_InId < SizeArray_OutId){
    EmptyNum = SizeArray_OutId - SizeArray_InId;
  }else{
    EmptyNum = SizeArraySize - SizeArray_InId + SizeArray_OutId;
  }
  //至少要空出一个容量
  if(EmptyNum<2){
    return -1;
  }

  Buffer_InId=fifo->Buffer_InId;
  Buffer_OutId=fifo->Buffer_OutId;
  BufferSize=fifo->BufferSize;
  if(Buffer_InId < Buffer_OutId){
    EmptySize = Buffer_OutId - Buffer_InId;
  }else{
    EmptySize = BufferSize - Buffer_InId;
    if(EmptySize < Buffer_OutId){
      EmptySize = Buffer_OutId;
    }
  }
  //容量是否足够
  if(EmptySize<Length){

    return -2;
  }

  //如果到此处 说明肯定可写入 要不从0写入要不从fifo->Buffer_InId写入

  //装填数据
  //判断从0写入还是从fifo->Buffer_InId写入
  src=Data;
  dst_ucp=fifo->Buffer;
  if((BufferSize - Buffer_InId)<Length){
    //从0写入

    i=Length;
    while(i>7){
      i-=8;

      dst_ucp[0]=src[0];
      dst_ucp[1]=src[1];
      dst_ucp[2]=src[2];
      dst_ucp[3]=src[3];
      dst_ucp[4]=src[4];
      dst_ucp[5]=src[5];
      dst_ucp[6]=src[6];
      dst_ucp[7]=src[7];

      dst_ucp=&dst_ucp[8];
      src=&src[8];
    }
    while(i>0){
      i--;

      dst_ucp[0]=src[0];

      dst_ucp=&dst_ucp[1];
      src=&src[1];
    }
    
    Buffer_InId=0;

  }else{
    //从fifo->Buffer_InId写入
    dst_ucp=&dst_ucp[Buffer_InId];

    i=Length;
    while(i>7){
      i-=8;

      dst_ucp[0]=src[0];
      dst_ucp[1]=src[1];
      dst_ucp[2]=src[2];
      dst_ucp[3]=src[3];
      dst_ucp[4]=src[4];
      dst_ucp[5]=src[5];
      dst_ucp[6]=src[6];
      dst_ucp[7]=src[7];

      dst_ucp=&dst_ucp[8];
      src=&src[8];
    }
    while(i>0){
      i--;

      dst_ucp[0]=src[0];

      dst_ucp=&dst_ucp[1];
      src=&src[1];
    }
  }

  if((Buffer_InId+Length)==BufferSize){
    fifo->Buffer_InId=0;
  }else{
    fifo->Buffer_InId=Buffer_InId+Length;
  }

  fifo->SizeArray[SizeArray_InId]=Length;

  if(SizeArray_InId==(SizeArraySize-1)){
    fifo->SizeArray_InId=0;
  }else{
    fifo->SizeArray_InId=SizeArray_InId+1;
  }

  return 0;
}

int H_FIFO_uint16_Write_Data0(H_FIFO* fifo,void* Data0,void* Data,int Length){
  H_FIFO_uint16_Type* src;
  H_FIFO_uint16_Type* dst_ucp;
  int EmptySize;
  int EmptyNum;
  int SizeArray_InId;
  int SizeArray_OutId;
  int Buffer_InId;
  int Buffer_OutId;
  int SizeArraySize;
  int BufferSize;
  int i;

  SizeArray_InId=fifo->SizeArray_InId;
  SizeArray_OutId=fifo->SizeArray_OutId;
  SizeArraySize=fifo->SizeArraySize;
  //判断是否有足够容量
  if(SizeArray_InId < SizeArray_OutId){
    EmptyNum = SizeArray_OutId - SizeArray_InId;
  }else{
    EmptyNum = SizeArraySize - SizeArray_InId + SizeArray_OutId;
  }
  //至少要空出一个容量
  if(EmptyNum<2){
    return -1;
  }

  Buffer_InId=fifo->Buffer_InId;
  Buffer_OutId=fifo->Buffer_OutId;
  BufferSize=fifo->BufferSize;
  if(Buffer_InId < Buffer_OutId){
    EmptySize = Buffer_OutId - Buffer_InId;
  }else{
    EmptySize = BufferSize - Buffer_InId;
    if(EmptySize < Buffer_OutId){
      EmptySize = Buffer_OutId;
    }
  }
  //容量是否足够
  if(EmptySize<(Length+1)){

    return -2;
  }

  //如果到此处 说明肯定可写入 要不从0写入要不从stream->Buffer_InId写入

  //装填数据
  //判断从0写入还是从fifo->Buffer_InId写入
  src=Data;
  dst_ucp=fifo->Buffer;
  if((BufferSize - Buffer_InId)<(Length+1)){
    //从0写入
    dst_ucp[0]=cH_FIFO_uint16_PtrReadByte(Data0);
    dst_ucp=&dst_ucp[1];

    i=Length;
    while(i>7){
      i-=8;

      dst_ucp[0]=src[0];
      dst_ucp[1]=src[1];
      dst_ucp[2]=src[2];
      dst_ucp[3]=src[3];
      dst_ucp[4]=src[4];
      dst_ucp[5]=src[5];
      dst_ucp[6]=src[6];
      dst_ucp[7]=src[7];

      dst_ucp=&dst_ucp[8];
      src=&src[8];
    }
    while(i>0){
      i--;

      dst_ucp[0]=src[0];

      dst_ucp=&dst_ucp[1];
      src=&src[1];
    }

    Buffer_InId=0;
  }else{
    //从fifo->Buffer_InId写入
    dst_ucp=&dst_ucp[Buffer_InId];
    dst_ucp[0]=cH_FIFO_uint16_PtrReadByte(Data0);
    dst_ucp=&dst_ucp[1];

    i=Length;
    while(i>7){
      i-=8;

      dst_ucp[0]=src[0];
      dst_ucp[1]=src[1];
      dst_ucp[2]=src[2];
      dst_ucp[3]=src[3];
      dst_ucp[4]=src[4];
      dst_ucp[5]=src[5];
      dst_ucp[6]=src[6];
      dst_ucp[7]=src[7];

      dst_ucp=&dst_ucp[8];
      src=&src[8];
    }
    while(i>0){
      i--;

      dst_ucp[0]=src[0];

      dst_ucp=&dst_ucp[1];
      src=&src[1];
    }
  }

  if((Buffer_InId+Length+1U)==BufferSize){
    fifo->Buffer_InId=0;
  }else{
    fifo->Buffer_InId=Buffer_InId+Length+1U;
  }

  fifo->SizeArray[SizeArray_InId]=Length+1U;

  if(SizeArray_InId==(SizeArraySize-1)){
    fifo->SizeArray_InId=0;
  }else{
    fifo->SizeArray_InId=SizeArray_InId+1;
  }

  return 0;
}

void* H_FIFO_uint16_Read(H_FIFO* fifo,int* LengthPtr){
  int PackSize;
  int Buffer_OutId;

  if(oH_FIFO_isEmpty(fifo)){
    PackSize=fifo->SizeArray[fifo->SizeArray_OutId];
    Buffer_OutId=fifo->Buffer_OutId;
    if((Buffer_OutId + PackSize) > fifo->BufferSize){
      fifo->Buffer_OutId=0;

      *LengthPtr=PackSize;
      return fifo->Buffer;
    }

    *LengthPtr=PackSize;
    return &((H_FIFO_uint16_Type*)fifo->Buffer)[Buffer_OutId];
  }
  else{
    *LengthPtr=0;
    return NULL;
  }
}





