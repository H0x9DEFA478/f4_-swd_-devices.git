/*
 * @Author: 0x9DEFA478
 * @Date: 2021-08-14 17:19:35
 * @LastEditTime: 2021-08-15 00:07:49
 * @LastEditors: 0x9DEFA478
 * @Description: 
 * QQ:2652450237
 * ============================================================================================================================================
 * 
 * 
 * 
 *                                                                                               ************      ****************************
 *                                                                                             ************      ****************************  
 *                                                                                           ************      ****************************    
 *                                                                                         ************      ****************************      
 *                                                                                       ************      ************                        
 *                                                                                     ************      ************                          
 *                                                                                   ************      ************                            
 *                                                                                 ************      ************                              
 *                                                                               ************      ************                                
 *                                                                             ************      ************                                  
 *                                                                           ************      ************                                    
 *                                                                         ************      ************                                      
 *                                                                       ************      ************                                        
 *                                                                     ************      ************                                          
 *                                                                   ************      ************                                            
 *                                                                 ************      ************                                              
 *                                                               ************      ************                                                
 *                                                             ************      ************                                                  
 *                                                           ************      ************                                                    
 *                                                         ************      ************                                                      
 *                                                       ************      ************                                                        
 *                                                     ************      ************                                                          
 *                                                   ************      ************                                                            
 *                                                 ************      ************                                                              
 *                                               ************      ************                                                                
 *                                             ************      ************                                                                  
 *                                           ************      ************                                                                    
 *                                         ************      ************                                                                      
 *                                       ************      ************                                                                        
 *                                     ************      ************                                                                          
 *                                   ************      ************                                                                            
 *                                 ************      ************                                                                              
 *                               ************      ************                                                                                
 *                             ************      ************                                                                                  
 *                           ************      ************                                                                                    
 *                         ************      ************                                                                                      
 *       ****************************      ************                                                                                        
 *     ****************************      ************                                                                                          
 *   ****************************      ************                                                                                            
 * ****************************      ************                                                                                              
 * 
 * 
 * 
 * ============================================================================================================================================
 * 
 */
#include "H_FIFO.h"



extern int H_FIFO_byte_Write(H_FIFO* fifo,void* Data,int Length);
extern int H_FIFO_byte_Write_Data0(H_FIFO* fifo,void* Data0,void* Data,int Length);
extern void* H_FIFO_byte_Read(H_FIFO* fifo,int* LengthPtr);

extern int H_FIFO_uint16_Write(H_FIFO* fifo,void* Data,int Length);
extern int H_FIFO_uint16_Write_Data0(H_FIFO* fifo,void* Data0,void* Data,int Length);
extern void* H_FIFO_uint16_Read(H_FIFO* fifo,int* LengthPtr);

extern int H_FIFO_uint32_Write(H_FIFO* fifo,void* Data,int Length);
extern int H_FIFO_uint32_Write_Data0(H_FIFO* fifo,void* Data0,void* Data,int Length);
extern void* H_FIFO_uint32_Read(H_FIFO* fifo,int* LengthPtr);

extern int H_FIFO_uint64_Write(H_FIFO* fifo,void* Data,int Length);
extern int H_FIFO_uint64_Write_Data0(H_FIFO* fifo,void* Data0,void* Data,int Length);
extern void* H_FIFO_uint64_Read(H_FIFO* fifo,int* LengthPtr);



/**
 * @brief 初始化FIFO
 * @param fifo 未使用过的FIFO句柄
 * @param Type FIFO输入/输出的数据类型 vH_FIFO_Type_byte vH_FIFO_Type_uint16 vH_FIFO_Type_uint32 vH_FIFO_Type_uint64
 * @param Buffer 指向用于缓存数据的内存的指针
 * @param BufferSize 缓存大小, 单位为Type指定的数据类型宽度
 * @param SizeArray 指向用于储存数据包大小的内存的指针
 * @param SizeArraySize SizeArray长度
 * @return 无
 */
void H_FIFO_Init(H_FIFO* fifo,int Type,void* Buffer,int BufferSize,int* SizeArray,int SizeArraySize){
  fifo->Buffer_InId=0;
	fifo->Buffer_OutId=0;
	fifo->SizeArray_InId=0;
	fifo->SizeArray_OutId=0;

	fifo->Buffer=Buffer;
	fifo->BufferSize=BufferSize;
	fifo->SizeArray=SizeArray;
	fifo->SizeArraySize=SizeArraySize;

  switch(Type){
    case vH_FIFO_Type_byte:
      fifo->Write=H_FIFO_byte_Write;
      fifo->Write_Data0=H_FIFO_byte_Write_Data0;
      fifo->Read=H_FIFO_byte_Read;
      break;
    case vH_FIFO_Type_uint16:
      fifo->Write=H_FIFO_uint16_Write;
      fifo->Write_Data0=H_FIFO_uint16_Write_Data0;
      fifo->Read=H_FIFO_uint16_Read;
      break;
    case vH_FIFO_Type_uint32:
      fifo->Write=H_FIFO_uint32_Write;
      fifo->Write_Data0=H_FIFO_uint32_Write_Data0;
      fifo->Read=H_FIFO_uint32_Read;
      break;
    case vH_FIFO_Type_uint64:
      fifo->Write=H_FIFO_uint64_Write;
      fifo->Write_Data0=H_FIFO_uint64_Write_Data0;
      fifo->Read=H_FIFO_uint64_Read;
      break;
    default:
      for(;;){
        //不支持的类型
      }
  }
}

/**
 * @brief FIFO是否不为空
 * @param fifo FIFO句柄
 * @return 0:FIFO空 其他:FIFO不为空
 */
int H_FIFO_isEmpty(H_FIFO* fifo){
  if(oH_FIFO_isEmpty(fifo)){
    return -1;
  }

  return 0;
}

/**
 * @brief 获取FIFO的当前最大可写入长度
 * @param fifo FIFO句柄
 * @return 当前可写入的最大数据长度(以fifo输入的数据类型位宽为单位)
 */
int H_FIFO_GetEmptySize(H_FIFO* fifo){
  int EmptySize;
  int EmptyNum;
  int SizeArray_InId;
  int SizeArray_OutId;
  int Buffer_InId;
  int Buffer_OutId;
  int SizeArraySize;
  int BufferSize;

  SizeArray_InId=fifo->SizeArray_InId;
  SizeArray_OutId=fifo->SizeArray_OutId;
  SizeArraySize=fifo->SizeArraySize;
  //计算容量
  if(SizeArray_InId < SizeArray_OutId){
    EmptyNum = SizeArray_OutId - SizeArray_InId;
  }else{
    EmptyNum = SizeArraySize - SizeArray_InId + SizeArray_OutId;
  }
  //至少要空出一个容量
  if(EmptyNum<2){
    return 0;
  }

  Buffer_InId=fifo->Buffer_InId;
  Buffer_OutId=fifo->Buffer_OutId;
  BufferSize=fifo->BufferSize;
  //计算空间
  if(Buffer_InId < Buffer_OutId){
    EmptySize = Buffer_OutId - Buffer_InId;
  }else{
    EmptySize = BufferSize - Buffer_InId;
    if(EmptySize < Buffer_OutId){
      EmptySize = Buffer_OutId;
    }
  }
  return EmptySize;
}

/**
 * @brief 读取数据指针切换到下一个 (必须在FIFO中有数据时使用, 即H_FIFO_Read()能调用成功的情况下)
 * @param fifo FIFO句柄
 * @return 无
 */
void H_FIFO_Next(H_FIFO* fifo){
  int PackSize;
  int Buffer_OutId;
  int SizeArray_OutId;

  SizeArray_OutId=fifo->SizeArray_OutId;
  PackSize=fifo->SizeArray[SizeArray_OutId];
  Buffer_OutId=fifo->Buffer_OutId;
  if((Buffer_OutId + PackSize) > fifo->BufferSize){
    fifo->Buffer_OutId=PackSize;
  }else{
    fifo->Buffer_OutId=Buffer_OutId+PackSize;
  }

  if(SizeArray_OutId==(fifo->SizeArraySize-1)){
    fifo->SizeArray_OutId=0;
  }else{
    fifo->SizeArray_OutId=SizeArray_OutId+1;
  }
}

/**
 * @brief 向FIFO中添加包
 * @param fifo FIFO句柄
 * @param Data 要添加的数据
 * @param Length 长度(以fifo输入的数据类型位宽为单位)
 * @return 0:成功 其他:失败 空间或剩余数量不足
 */
int H_FIFO_Write(H_FIFO* fifo,void* Data,int Length){
  return fifo->Write(fifo,Data,Length);
}

/**
 * @brief 向FIFO中添加包(头部插入一个数据)
 * @param fifo FIFO句柄
 * @param Data0 头部插入的数据(位宽与fifo输入的数据类型一致)
 * @param Data 要添加的数据
 * @param Length 长度(以fifo输入的数据类型位宽为单位)
 * @return 0:成功 其他:失败 空间或剩余数量不足
 */
int H_FIFO_Write_Data0(H_FIFO* fifo,void* Data0,void* Data,int Length){
  return fifo->Write_Data0(fifo,Data0,Data,Length);
}

/**
 * @brief 读取输出数据指针 处理完数据后 调用H_FIFO_Next()来释放读取到的数据包
 * @param fifo FIFO句柄
 * @param LengthPtr 用于返回数据包大小
 * @return 读取数据的指针 如果为NULL((void*)0)表示无可用数据
 */
void* H_FIFO_Read(H_FIFO* fifo,int* LengthPtr){
  return fifo->Read(fifo,LengthPtr);
}



