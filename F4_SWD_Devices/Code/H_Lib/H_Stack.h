/*
 * @Author: 0x9DEFA478
 * @Date: 2021-04-09 15:56:00
 * @LastEditTime: 2021-04-21 18:20:28
 * @LastEditors: 0x9DEFA478
 * @Description: 说明见.C
 * QQ:2652450237
 * ============================================================================================================================================
 * 
 * 
 * 
 *                                                                                               ************      ****************************
 *                                                                                             ************      ****************************  
 *                                                                                           ************      ****************************    
 *                                                                                         ************      ****************************      
 *                                                                                       ************      ************                        
 *                                                                                     ************      ************                          
 *                                                                                   ************      ************                            
 *                                                                                 ************      ************                              
 *                                                                               ************      ************                                
 *                                                                             ************      ************                                  
 *                                                                           ************      ************                                    
 *                                                                         ************      ************                                      
 *                                                                       ************      ************                                        
 *                                                                     ************      ************                                          
 *                                                                   ************      ************                                            
 *                                                                 ************      ************                                              
 *                                                               ************      ************                                                
 *                                                             ************      ************                                                  
 *                                                           ************      ************                                                    
 *                                                         ************      ************                                                      
 *                                                       ************      ************                                                        
 *                                                     ************      ************                                                          
 *                                                   ************      ************                                                            
 *                                                 ************      ************                                                              
 *                                               ************      ************                                                                
 *                                             ************      ************                                                                  
 *                                           ************      ************                                                                    
 *                                         ************      ************                                                                      
 *                                       ************      ************                                                                        
 *                                     ************      ************                                                                          
 *                                   ************      ************                                                                            
 *                                 ************      ************                                                                              
 *                               ************      ************                                                                                
 *                             ************      ************                                                                                  
 *                           ************      ************                                                                                    
 *                         ************      ************                                                                                      
 *       ****************************      ************                                                                                        
 *     ****************************      ************                                                                                          
 *   ****************************      ************                                                                                            
 * ****************************      ************                                                                                              
 * 
 * 
 * 
 * ============================================================================================================================================
 * 
 */

#ifndef __H_Stack_H_
#define __H_Stack_H_

#include "H_Type.h"

typedef struct
{
  void** Items;
  int pointer;
  int NumOfItem;
}H_Stack_voidPtr_Def;



/**
 * @brief 初始化先进后出栈
 * @param stack 栈实例
 * @param Items 所使用的内存空间
 * @param NumOfItem 项数量
 * @return 无
 */
void H_Stack_voidPtr_Init(H_Stack_voidPtr_Def* stack,void** Items,int NumOfItem);

/**
 * @brief 将项压栈
 * @param stack 栈实例
 * @param item 项
 * @return 0:成功 其他:失败 堆栈满
 */
int H_Stack_voidPtr_Push(H_Stack_voidPtr_Def* stack,void* item);

/**
 * @brief 出栈
 * @param stack 栈实例
 * @return 项 如果为NULL 说明栈已空
 */
void* H_Stack_voidPtr_Pop(H_Stack_voidPtr_Def* stack);

/**
 * @brief 清空堆栈
 * @param stack 栈实例
 * @return 无
 */
void H_Stack_voidPtr_Clear(H_Stack_voidPtr_Def* stack);


#endif
